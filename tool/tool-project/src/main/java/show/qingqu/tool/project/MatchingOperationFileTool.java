/**
 *
 */

package show.qingqu.tool.project;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * @author kam
 *
 * @Description: 根据规则操作项目内某些文件的工具
 */
public class MatchingOperationFileTool {

    public static void main(String[] args) {
        //匹配条件
        Predicate<File> filePredicate = file -> Objects.equals(file.getName(), "application-prod.yml") || Objects.equals(file.getName(), "application-test.yml");

        //对文件的操作
        Consumer<File> fileConsumer = file -> {
            if (filePredicate.test(file)) {
                System.out.println("删除了：" + file.getAbsolutePath());
                file.delete();
            }
        };

        File file = new File("");
        File projectDirFile = file.getAbsoluteFile();
        findFile(projectDirFile, fileConsumer);
    }

    private static void findFile(File file, Consumer<? super File> fileConsumer) {
        if (Objects.isNull(file)) {
            return;
        }
        if (file.exists() && file.isDirectory()) {
            Arrays.stream(Objects.requireNonNull(file.listFiles())).forEach(sonFile -> {
                findFile(sonFile, fileConsumer);
            });
        } else {
            fileConsumer.accept(file);
        }
    }


}
