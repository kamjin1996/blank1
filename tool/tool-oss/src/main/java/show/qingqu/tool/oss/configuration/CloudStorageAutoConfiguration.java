package show.qingqu.tool.oss.configuration;


import show.qingqu.tool.oss.constants.OSSConstant;
import show.qingqu.tool.oss.service.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author kam
 *
 * @Description: 自动装配
 */
@Configuration
@EnableConfigurationProperties(CloudStorageProperties.class)
public class CloudStorageAutoConfiguration {

    /**
     * 七牛token
     *
     * @param config
     * @return
     */
    @Bean
    @ConditionalOnProperty(value = "oss.type", havingValue = OSSConstant.TYPE_QINIU)
    public QiniuToken qiniuToken(CloudStorageProperties config) {
        return new QiniuToken(config);
    }

    /**
     * 文件上传
     *
     * @param config
     * @return
     */
    @Bean
    @ConditionalOnProperty(value = "oss.type")
    public CloudStorageService CloudStorageService(CloudStorageProperties config, QiniuToken qiniuToken) {
        if (config.getType().equals(OSSConstant.TYPE_QINIU)) {
            return new QiniuCloudStorageService(config, qiniuToken);
        } else if (config.getType().equals(OSSConstant.TYPE_ALIYUN)) {
            return new AliyunCloudStorageService(config);
        } else if (config.getType().equals(OSSConstant.TYPE_QCLOUD)) {
            return new QcloudCloudStorageService(config);
        }
        return null;
    }


}
