
package show.qingqu.tool.oss.constants;

/**
 * @author kam
 * @create 2018/3/4.
 */
public class OSSConstant {
    //类型 1：七牛  2：阿里云  3：腾讯云
    public final static String TYPE_QINIU = "1";
    public final static String TYPE_ALIYUN = "2";
    public final static String TYPE_QCLOUD = "3";
}
