
package show.qingqu.tool.oss.controller;

import show.qingqu.tool.oss.service.CloudStorageService;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


/**
 * 文件上传
 *
 * @author kam
 */
//@RestController
//@RequestMapping("/oss")
public class OssController {

    /**
     * 文件不存在
     */
    private CloudStorageService cloudStorageService;


    /**
     * 上传文件
     */
//    @RequestMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RuntimeException("上传文件不能为空");
        }
        //上传文件
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        return cloudStorageService.uploadSuffix(file.getBytes(), suffix);
    }

}
