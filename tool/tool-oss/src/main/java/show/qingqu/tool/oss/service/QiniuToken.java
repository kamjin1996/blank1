package show.qingqu.tool.oss.service;

import show.qingqu.tool.oss.configuration.CloudStorageProperties;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 七牛上传凭证
 *
 * @author kam
 */
@Slf4j
public class QiniuToken implements Serializable {

    /**
     * 配置
     */
    private CloudStorageProperties config;

    /**
     * token
     */
    private String upToken;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 凭证有效期 秒
     */
    private Long expireSeconds = 86400L;

    /**
     * 上传路径前缀
     */
    private String prefix;

    /**
     * 下载文件路径
     */
    private String domain;

    public QiniuToken(CloudStorageProperties config) {
        this.config = config;
        this.domain = config.getQiniuDomain();
        this.prefix = config.getQiniuPrefix();
    }

    /**
     * 刷新token
     */
    public synchronized void refreshUpToken() {
        this.createTime = new Date();
        Auth auth = Auth.create(config.getQiniuAccessKey(), config.getQiniuSecretKey());
        this.upToken = auth.uploadToken(config.getQiniuBucketName(), null, this.expireSeconds, this.putPolicy());
        log.info("\n【七牛云】刷新upToken \n【七牛云】upToken：{} \n【七牛云】创建时间：{} \n【七牛云】有效期：{}秒", this.upToken,
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.createTime), this.expireSeconds);
    }

    /**
     * 自定义响应变量 <a href="https://developer.qiniu.com/kodo/manual/1235/vars#magicvar"/>
     *
     * @return
     */
    private StringMap putPolicy() {
        JSONObject data = new JSONObject();
        data.put("key", "$(key)");
        data.put("hash", "$(etag)");
        data.put("bucket", "$(bucket)");
        data.put("fsize", "$(fsize)");
        data.put("mimeType", "$(mimeType)");
//      data.put("imageInfo", "$(imageInfo)");  // 有bug  返回值为string

        JSONObject result = new JSONObject();
        result.put("code", "200");
        result.put("msg", "success");
        result.put("data", data);
        StringMap putPolicy = new StringMap();
        putPolicy.put("returnBody", result.toString());
        return putPolicy;
    }


    /**
     * 判断token有效期
     *
     * @return true 有效 / false 无效
     */
    public boolean checkExpire() {
        if (StringUtils.isBlank(this.upToken)) return false;
        long slotTime = 1800 * 1000; // 间隙时间 给予半小时间隙时间
        long time = this.createTime.getTime();
        long expireTime = (time + (this.expireSeconds * 1000)) - slotTime; // 失效期时间戳
        long currentTime = System.currentTimeMillis(); // 当前时间戳
        return currentTime <= expireTime;
    }

    public String getUpToken() {
        if (!this.checkExpire()) {
            this.refreshUpToken();
        }
        return upToken;
    }

    public void setUpToken(String upToken) {
        this.upToken = upToken;
    }

    public CloudStorageProperties getConfig() {
        return config;
    }

    public void setConfig(CloudStorageProperties config) {
        this.config = config;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getExpireSeconds() {
        return expireSeconds;
    }

    public void setExpireSeconds(Long expireSeconds) {
        this.expireSeconds = expireSeconds;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public static Logger getLog() {
        return log;
    }
}
