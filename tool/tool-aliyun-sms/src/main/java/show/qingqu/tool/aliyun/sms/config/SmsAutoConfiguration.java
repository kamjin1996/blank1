package show.qingqu.tool.aliyun.sms.config;

import show.qingqu.tool.aliyun.sms.Sms;
import show.qingqu.tool.aliyun.sms.SmsService;
import show.qingqu.tool.aliyun.sms.impl.SmsImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(SmsProperties.class)
public class SmsAutoConfiguration {

    /**
     * 短信发送service
     *
     * @param config
     * @return
     */
    @Bean
    @ConditionalOnProperty(value = "sms.aliyun.access-key-id")
    public SmsService SmsService(SmsProperties config) {
        Sms sms = new SmsImpl(config);
        SmsService smsService = new SmsService();
        smsService.setSms(sms);
        return smsService;
    }

}
