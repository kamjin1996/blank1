package show.qingqu.tool.aliyun.sms;

import show.qingqu.common.sms.SmsRequest;
import show.qingqu.tool.aliyun.sms.bean.SmsResult;

/**
 * 消息发送
 *
 * @author kam
 */
public interface Sms {

    /**
     * 发送短信
     *
     * @param request
     * @return
     */
    SmsResult sendSmsPre(SmsRequest request);

}
