package show.qingqu.tool.aliyun.sms.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 验证码
 *
 * @author kam
 */
@Data
public class VerifCode implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 验证码
     */
    private String code;
    /**
     * 发送时间
     */
    private Date sendTime;

    /**
     * 从新发送间隔 单位 :秒 （默认60秒）
     */
    private int repeatExpire;

    /**
     * 失效时间 单位：秒（默认5分钟）
     */
    private int abateExpire;

    /**
     * 发送至手机号码
     */
    private String mobile;

    /**
     * IP地址
     */
    private String ip;


    public VerifCode() {
    }

    /**
     * 重复发送间隔默认60秒
     *
     * @param code
     */
    public VerifCode(String code, String mobile) {
        super();
        this.sendTime = new Date();
        this.code = code;
        this.mobile = mobile;
        this.abateExpire = 5 * 60;
        this.repeatExpire = 60;
    }

    public VerifCode(String code, String mobile, int repeatExpire) {
        super();
        this.code = code;
        this.mobile = mobile;
        this.sendTime = new Date();
        this.repeatExpire = repeatExpire;
    }

    /**
     * 判断验证码有效期
     *
     * @return
     */
    public boolean checkAbateExpire() {
        long time = this.sendTime.getTime();
        long expireTime = time + (getAbateExpire() * 1000); // 失效期时间戳
        long currentTime = System.currentTimeMillis(); // 当前时间戳
        return currentTime <= expireTime;
    }

    /**
     * 判断是否可以再次发送
     *
     * @return
     */
    public boolean checkRepeatExpire() {
        long time = this.sendTime.getTime();
        long expireTime = time + ((getRepeatExpire() - 2) * 1000); // 失效期时间戳 2秒容错
        long currentTime = System.currentTimeMillis(); // 当前时间戳
        return currentTime <= expireTime;
    }
}
