package show.qingqu.tool.aliyun.sms;

import show.qingqu.common.sms.AliyunSmsTemplateEnum;
import show.qingqu.common.sms.SmsRequest;
import show.qingqu.tool.aliyun.sms.bean.SmsResult;
import show.qingqu.tool.aliyun.sms.bean.VerifCode;
import show.qingqu.tool.aliyun.sms.utils.PhoneUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.util.Objects;

/**
 * 短信服务
 *
 * @author kam
 */
@Data
@Slf4j
public class SmsService {

    public Sms sms;

    /**
     * 发送短信验证码
     *
     * @param code 短信包装
     * @return
     */
    public SmsResult sendVerifCode(VerifCode code) {
        return this.sendVerifCode(code.getMobile(), code.getCode());
    }

    /**
     * 发送短信验证码
     *
     * @param mobile 手机号码
     * @param code   短信
     * @return
     */
    public SmsResult sendVerifCode(String mobile, String code) {
        Assert.notNull(mobile, "手机号码不能为空");
        Assert.notNull(code, "验证码不能为空");
        if (!PhoneUtil.checkMobile(mobile)) {
            throw new IllegalArgumentException("手机号码格式不正确");
        }
        log.debug("\n【短信发送】:发送成功 mobile:{}，code:{}", mobile, code);
        SmsRequest request = new SmsRequest();
        request.setMobile(mobile);
        request.setCode(code);
        request.setSmsType(AliyunSmsTemplateEnum.VERIF_CODE);
        return this.sms.sendSmsPre(request);
    }

    /**
     * 发送商家审核状态
     *
     * @param request
     * @return
     */
    public SmsResult sendSellerAuthStatus(SmsRequest request) {
        Assert.notNull(request.getMobile(), "手机号码不能为空");
        if (!PhoneUtil.checkMobile(request.getMobile())) {
            throw new IllegalArgumentException("手机号码格式不正确");
        }
        log.debug("\n【短信发送】:发送成功 mobile:{}", request.getMobile());
        request.setSmsType(AliyunSmsTemplateEnum.SELLER_AUTH_NOTIFICATION);
        return this.sms.sendSmsPre(request);
    }

    public SmsResult sendRepairAuthNotification(SmsRequest request) {
        Assert.notNull(request.getMobile(), "手机号码不能为空");
        if (!PhoneUtil.checkMobile(request.getMobile())) {
            throw new IllegalArgumentException("手机号码格式不正确");
        }
        if (request.getStatus().equals("FAILURE")) {
            request.setSmsType(AliyunSmsTemplateEnum.REPAIR_FACTORY_AUTH_SUCCESS);
        }
        if (request.getStatus().equals("OK")) {
            request.setSmsType(AliyunSmsTemplateEnum.REPAIR_FACTORY_AUTH_FAILURE);
        }
        return this.sms.sendSmsPre(request);
    }


    /**
     * 获取短信发送验证码对象
     *
     * @param mobile
     * @return
     */
    public VerifCode verifCode(String mobile) {
        String code = String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
        return new VerifCode(code, mobile);
    }

    /**
     * 发送短信提醒
     *
     * @param request
     * @return
     */
    public SmsResult sendNotification(SmsRequest request) {
        String mobile = request.getMobile();
        Assert.notNull(mobile, "手机号码不能为空");
        if (!PhoneUtil.checkMobile(mobile)) {
            throw new IllegalArgumentException("手机号码格式不正确");
        }
        if (Objects.isNull(request.getSmsType())) {
            throw new IllegalArgumentException("短信模板ID不能为空");
        }
        log.debug("\n【短信发送】:短信提醒发送成功 mobile:{}", mobile);
        return this.sms.sendSmsPre(request);
    }

}
