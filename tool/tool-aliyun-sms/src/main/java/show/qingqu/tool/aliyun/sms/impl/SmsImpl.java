package show.qingqu.tool.aliyun.sms.impl;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import show.qingqu.common.sms.AliyunSmsTemplateEnum;
import show.qingqu.common.sms.SmsRequest;
import show.qingqu.tool.aliyun.sms.Sms;
import show.qingqu.tool.aliyun.sms.bean.SmsResult;
import show.qingqu.tool.aliyun.sms.config.SmsProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@Slf4j
public class SmsImpl implements Sms {

    @Autowired
    public SmsProperties smsProperties;

    public SmsImpl() {
    }

    public SmsImpl(SmsProperties smsProperties) {
        this.smsProperties = smsProperties;
    }


    /**
     * 发送短信
     *
     * @return
     */
    public SmsResult sendSms(SendSmsRequest request) {
        // AK
        final String accessKeyId = this.getSmsProperties().getAccessKeyId();
        final String accessKeySecret = this.getSmsProperties().getAccessKeySecret();
        // 初始化ascClient需要的几个参数
        final String product = "Dysmsapi"; // 不变
        final String domain = "dysmsapi.aliyuncs.com";// 地址不变

        try {
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            DefaultAcsClient acsClient = new DefaultAcsClient(profile);
            // 发送
            SendSmsResponse response = acsClient.getAcsResponse(request);
            log.debug("\n【短信发送】发送结果  \n【短信发送】Code:{} \n【短信发送】msg:{} \n【短信发送】requestId:{} \n【短信发送】bizId:{}", response.getCode(), response.getMessage(), response.getRequestId(), response.getBizId());
            return new SmsResult(response);
        } catch (ClientException e) {
            log.error("\n【短信发送】发送失败 \n【短信发送】RequestId:{} \n【短信发送】Code:{} \n【短信发送】ErrorMessage:{} \n【短信发送】ErrorType:{}",
                    e.getRequestId(), e.getErrCode(), e.getErrMsg(), e.getErrorType());
            return new SmsResult(e);
        }
    }


    @Override
    public SmsResult sendSmsPre(SmsRequest smsRequest) {

        final String SignName = this.getSmsProperties().getSignName();
        // 设置超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        // 组装请求对象
        SendSmsRequest request = new SendSmsRequest();
        request.setMethod(MethodType.POST);
        request.setPhoneNumbers(smsRequest.getMobile()); // 必填:手机号码
        request.setSignName(SignName);   //必填:短信签名
        request.setTemplateCode(this.getSmsProperties().getTemplateCode().get(smsRequest.getSmsType().getTitle()));// 必填:短信模板
        if (smsRequest.getSmsType().getTitle().equals(AliyunSmsTemplateEnum.VERIF_CODE.getTitle())) {
            // 发送验证码
            request.setTemplateParam("{\"code\":\"" + smsRequest.getCode() + "\"}"); // 变量
        } else if (smsRequest.getSmsType().getTitle().equals(AliyunSmsTemplateEnum.SELLER_AUTH_NOTIFICATION.getTitle())) {
            // 发送商家认证审核
            request.setTemplateParam("{\"auditType\":\"" + smsRequest.getAuditType() + "\",\"status\":\"" + smsRequest.getStatus() + "\"}"); // 变量——审核类型
        } else if (smsRequest.getSmsType().getTitle().equals(AliyunSmsTemplateEnum.REPAIR_FACTORY_AUTH_FAILURE.getTitle())) {
            // 修理厂认证失败
            request.setTemplateParam("{\"reason\":\"" + smsRequest.getReason() + "\"}");
        } else if (smsRequest.getSmsType().getTitle().equals(AliyunSmsTemplateEnum.FINANCIAL_DRAW_REMINDER.getTitle())) {
            // 财务提现提醒
            request.setTemplateParam("{\"name\":\"" + smsRequest.getDrawRecord().getUserName() + "\"}");
            request.setTemplateParam("{\"phone\":\"" + smsRequest.getDrawRecord().getPhone() + "\"}");
            request.setTemplateParam("{\"money\":\"" + smsRequest.getDrawRecord().getMoney() + "\"}");
        } else if (smsRequest.getSmsType().getTitle().equals(AliyunSmsTemplateEnum.PAY_SUCCESS_SELLER_NOTIFACATION.getTitle())) {
            // 买家支付成功 卖家提醒
            request.setTemplateParam("{\"buyer\":\"" + smsRequest.getPaySuccessSeller().getBuyer() + "\"}");
            request.setTemplateParam("{\"seller\":\"" + smsRequest.getPaySuccessSeller().getSeller() + "\"}");
            request.setTemplateParam("{\"money\":\"" + smsRequest.getPaySuccessSeller().getMoney() + "\"}");
        }
        return this.sendSms(request);
    }


}
