package show.qingqu.tool.aliyun.sms.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * 短信发送
 *
 * @author kam
 */
@Data
@ConfigurationProperties(prefix = "sms.aliyun")
public class SmsProperties {
    /**
     * 模板code
     */

    private Map<String, String> templateCode = new TreeMap<>();

    /**
     * 签名名称
     */
    private String signName;

    /**
     * AccessKeyID
     */
    private String accessKeyId;

    /**
     * AccessKeySecret
     */
    private String accessKeySecret;

}
