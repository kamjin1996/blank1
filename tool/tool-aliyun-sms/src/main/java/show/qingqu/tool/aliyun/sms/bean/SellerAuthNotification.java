package show.qingqu.tool.aliyun.sms.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * 商家审核短信
 *
 * @author kam
  *
 */
@Data
public class SellerAuthNotification implements Serializable {

    private static final long serialVersionUID = -1979451920137810186L;


    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 状态
     */
    private String status;

    /**
     * 认证类型
     */
    private String auditType;

    /**
     * 原因
     */
    private String reason;

    /**
     * IP地址
     */
    private String ip;

}
