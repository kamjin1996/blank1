package show.qingqu.user.domain.model;

import com.baomidou.mybatisplus.annotation.TableField;
import show.qingqu.common.mysql.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表（测试）
 * </p>
 *
 * @author kam
 * @since 2020-03-31
 */
@Data
@ApiModel("用户表（测试）")
public class User extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名称
     */
    @TableField("name")
    @ApiModelProperty("用户名称")
    private String name;

    /**
     * 名称
     */
    @TableField("age")
    @ApiModelProperty("名称")
    private Integer age;

    /**
     * 账户名称
     */
     @TableField("username")
    @ApiModelProperty("账户名称")
    private String username;

    /**
     * 用户密码
     */
     @TableField("age")
    @ApiModelProperty("用户密码")
    private String password;


}
