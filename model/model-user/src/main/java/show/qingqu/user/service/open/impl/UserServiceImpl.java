package show.qingqu.user.service.open.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import show.qingqu.common.exception.ExceptionFactory;
import show.qingqu.user.domain.model.User;
import show.qingqu.user.domain.query.UserQuery;
import show.qingqu.user.domain.request.UserLoginReqeust;
import show.qingqu.user.domain.response.LoginReponse;
import show.qingqu.user.mapper.UserMapper;
import show.qingqu.user.service.open.UserService;
import show.qingqu.common.mysql.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import show.qingqu.user.utils.LoginHelp;

import java.util.Objects;

/**
 * <p>
 * 用户表（测试） 服务实现类
 * </p>
 *
 * @author kam
 * @since 2020-03-31
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements UserService {

    @Override
    public User findById(Long id) {
        return this.getById(id);
    }

    @Override
    public Page<User> findByPage(UserQuery query) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        return this.page(query.getPage(), wrapper);
    }

    @Override
    public void delete(Long id) {
        this.removeById(id);
    }

    @Override
    public LoginReponse login(UserLoginReqeust reqeust) {
        User user = new User();
        user.setUsername(reqeust.getUsername());

        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
        User one = this.getOne(wrapper);
        if (Objects.isNull(one)) {
             ExceptionFactory.build("用户不存在");
             user= null;
        }

        if (!Objects.equals(reqeust.getPassword(), one.getPassword())) {
             ExceptionFactory.build("密码不正确");
             user = null;
        }
        //放入token
        return LoginHelp.loginByUser(one);

    }

    @Override
    public boolean add(User user) {
        return this.save(user);
    }

    @Override
    public boolean update(User user) {
        return this.updateById(user);
    }

}
