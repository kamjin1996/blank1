package show.qingqu.user.domain.query;

import show.qingqu.user.domain.model.User;
import show.qingqu.common.mysql.page.PageHelp;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 * 用户表（测试）分页查询
 * </p>
 *
 * @author kam
 * @since 2020-03-31
 */
@Data
@ApiModel("用户表（测试）分页查询")
public class UserQuery extends PageHelp<User> {

    private static final long serialVersionUID = 1L;


}
