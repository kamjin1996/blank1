package show.qingqu.user.utils;

/**
 * @author kam
 *
 * <p>
 *
 * </p>
 */

import show.qingqu.common.security.JwtToken;
import show.qingqu.common.security.JwtTokenUtil;
import show.qingqu.common.security.JwtUser;
import show.qingqu.user.domain.model.User;
import show.qingqu.user.domain.response.LoginReponse;

import java.util.UUID;

/**
 * 登录帮助类
 */
public final class LoginHelp {
    /**
     * 根据系统用户信息生成token
     *
     * @param user
     * @return
     */
    public static LoginReponse loginByUser(User user) {
        JwtToken jwtToken = JwtTokenUtil.generateToken(new JwtUser(user.getId(), user.getUsername()));
        return new LoginReponse(user, jwtToken);
    }


    /**
     * 生成随机字符串
     *
     * @return
     */
    public static String generateStr() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }

}
