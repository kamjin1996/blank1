package show.qingqu.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import show.qingqu.user.domain.model.User;

/**
 * <p>
 * 用户表（测试） Mapper 接口
 * </p>
 *
 * @author kam
 * @since 2020-03-31
 */
public interface UserMapper extends BaseMapper<User> {

}
