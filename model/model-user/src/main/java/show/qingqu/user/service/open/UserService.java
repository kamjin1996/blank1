package show.qingqu.user.service.open;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import show.qingqu.user.domain.model.User;
import show.qingqu.user.domain.query.UserQuery;
import show.qingqu.common.mysql.service.BaseService;
import show.qingqu.user.domain.request.UserLoginReqeust;
import show.qingqu.user.domain.response.LoginReponse;

/**
 * <p>
 * 用户表（测试） 服务类
 * </p>
 *
 * @author kam
 * @since 2020-03-31
 */
public interface UserService extends BaseService<User> {

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    User findById(Long id);

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    Page<User> findByPage(UserQuery query);

    /**
     * 新增
     *
     * @param user
     */
    boolean add(User user);

    /**
     * 修改
     *
     * @param user
     * @return
     */
    boolean update(User user);

    /**
     * 根据ID删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 登录
     *
     * @param reqeust
     */
    LoginReponse login(UserLoginReqeust reqeust);
}
