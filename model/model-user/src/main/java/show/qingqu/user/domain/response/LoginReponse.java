package show.qingqu.user.domain.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import show.qingqu.common.security.JwtToken;
import show.qingqu.user.domain.model.User;

import java.io.Serializable;

/**
 * 登录返回用户信息
 */
@Data
@ApiModel("登录返回信息")
public class LoginReponse implements Serializable {


    /**
     * 账户名
     */
    private String username;

    /**
     * token相关信息
     */
    @ApiModelProperty("token相关信息")
    private JwtToken jwtToken;

    public LoginReponse() {
    }

    public LoginReponse(User user, JwtToken jwtToken) {
        this.username = user.getUsername();
        this.jwtToken = jwtToken;
    }
}
