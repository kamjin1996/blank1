package show.qingqu.user.controller.open.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import show.qingqu.common.result.Result;
import show.qingqu.user.domain.query.UserQuery;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import show.qingqu.user.domain.model.User;
import show.qingqu.user.domain.request.UserLoginReqeust;
import show.qingqu.user.domain.response.LoginReponse;
import show.qingqu.user.service.open.UserService;

import org.springframework.web.bind.annotation.RestController;
import show.qingqu.common.controller.BaseController;

/**
 * <p>
 * 用户表（测试） 前端控制器
 * </p>
 *
 * @author kam
 * @since 2020-03-31
 */
@RestController
@RequestMapping("/user")
@Api(value = "UserController", tags = "【用户表（测试）】")
public class UserController extends BaseController {

    @Autowired
    public UserService userService;

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "根据ID查询", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<User> loadById(@PathVariable(value = "id") Long id) {
        return response(this.userService.findById(id));
    }

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @GetMapping(value = "/page", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "总分页查询", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Page<User>> loadByPage(UserQuery query) {
        return response(this.userService.findByPage(query));
    }

    /**
     * 新增
     *
     * @param user
     * @return
     */
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "新增", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Boolean> add(@RequestBody User user) {
        return response(this.userService.add(user));
    }

    /**
     * 修改
     *
     * @param user
     * @return
     */
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "修改", httpMethod = "PUT", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Boolean> update(@RequestBody User user) {
        return response(this.userService.update(user));
    }

    /**
     * 根据ID删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "根据ID删除", httpMethod = "DELETE", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Object> delete(@PathVariable(value = "id") Long id) {
        this.userService.delete(id);
        return response();
    }

    /**
     * 用户登录
     *
     * @param reqeust
     * @return
     */
    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "用户登录", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<LoginReponse> login(@RequestBody UserLoginReqeust reqeust) {
        return response(this.userService.login(reqeust));
    }

}

