package show.qingqu.user.domain.request;

import lombok.Data;

/**
 * @author kam
 *
 * <p>
 * 用户登录
 * </p>
 */
@Data
public class UserLoginReqeust {

    /**
     * 名称
     */
    private String username;

    /**
     * 密码
     */
    private String password;
}
