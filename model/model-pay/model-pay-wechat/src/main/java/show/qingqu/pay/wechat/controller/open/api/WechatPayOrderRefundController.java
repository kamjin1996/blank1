package show.qingqu.pay.wechat.controller.open.api;

import show.qingqu.common.result.Result;
import show.qingqu.pay.wechat.domain.enums.PayOrderStatus;
import show.qingqu.pay.wechat.domain.model.WechatPayOrder;
import show.qingqu.pay.wechat.domain.request.RefundRequest;
import show.qingqu.pay.wechat.service.open.WechatPayOrderService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import show.qingqu.pay.wechat.service.open.WechatPayOrderRefundService;

import org.springframework.web.bind.annotation.RestController;
import show.qingqu.common.controller.BaseController;

import java.util.Objects;

/**
 * <p>
 * 微信支付退款 前端控制器
 * </p>
 *
 * @author kam
 * @since 2019-01-10
 */
@RestController
@RequestMapping("/wechat/pay/order/refund")
@Api(value = "WechatPayOrderRefundController", tags = "【微信支付退款】")
public class WechatPayOrderRefundController extends BaseController {

    @Autowired
    public WechatPayOrderRefundService wechatPayOrderRefundService;

    @Autowired
    private WechatPayOrderService wechatPayOrderService;

    /**
     * 微信订单退款
     *
     * @return
     */
    @PostMapping(value = "/refund", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "微信订单退款", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Object> refund(@RequestBody RefundRequest request) {
        WechatPayOrder payOrder = this.wechatPayOrderService.findByOutTradeNo(request.getOutTradeNo());
        if (Objects.isNull(payOrder)) {
            return responseMsg("订单不存在");
        }
        if (!payOrder.getStatus().equals(PayOrderStatus.SUCCESS)) {
            return responseMsg("订单未支付无法申请退款");
        }
        long totalRefundFee = this.wechatPayOrderRefundService.sumTotalRefundFeeByOutTradeNo(payOrder.getOutTradeNo());
        long surRefundFee = payOrder.getTotalFee() - totalRefundFee;
        if (surRefundFee < request.getRefundFee()) {
            return responseMsg("订单剩余可退款金额不足");
        }
        this.wechatPayOrderRefundService.refund(request);
        return response();
    }
}

