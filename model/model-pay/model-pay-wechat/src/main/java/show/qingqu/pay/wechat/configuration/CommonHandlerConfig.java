package show.qingqu.pay.wechat.configuration;

import show.qingqu.common.configuration.CorsConfig;
import show.qingqu.common.exception.handler.RestNotFoundController;
import show.qingqu.common.security.exception.WebSecurityExceptionHandlerAdvice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author kam
 *
 * @Description: 全局基础配置bean
 */
@Slf4j
@Configuration
public class CommonHandlerConfig {

    @Bean
    @ConditionalOnClass(WebSecurityExceptionHandlerAdvice.class)
    public WebSecurityExceptionHandlerAdvice webSecurityExceptionHandlerAdviceebSecurityExceptionHandlerAdvice() {
        return new WebSecurityExceptionHandlerAdvice();
    }

    @Bean
    @ConditionalOnClass(RestNotFoundController.class)
    public RestNotFoundController RestNotFoundController() {
        return new RestNotFoundController();
    }

    @Bean
    @ConditionalOnClass(CorsConfig.class)
    public CorsConfig CorsConfig() {
        return new CorsConfig();
    }


}
