package show.qingqu.pay.wechat.domain.enums;

import lombok.Getter;

/**
 * @author kam

 * 具体订单类型
 **/
@Getter
public enum OrderDetailType {

    /**
     * 支付
     */
    PAY(0),

    /**
     * 退款
     */
    REFUND(1),

    /**
     * 提现到微信零钱
     */
    WITHDRAW_CHANGE(2),


    /**
     * 提现到银行卡
     */
    WITHDRAW_BANK(3);

    private int type;

    OrderDetailType(int type) {
        this.type = type;
    }
}
