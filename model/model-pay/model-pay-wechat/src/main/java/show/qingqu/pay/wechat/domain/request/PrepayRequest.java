package show.qingqu.pay.wechat.domain.request;

import show.qingqu.pay.wechat.domain.enums.ClientType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author kam

 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("支付信息")
public class PrepayRequest implements Serializable {

    @ApiModelProperty("订单号,支付成功回调使用")
    private String tradeNo;

    @ApiModelProperty("订单类型,支付成功回调使用")
    private OrderType orderType;

    @ApiModelProperty("订单金额")
    private Long totalFee;

    @ApiModelProperty("订单标题")
    private String subject = "ZCXY在线交易";

    @ApiModelProperty("客户端类型")
    private ClientType clientType;

    @ApiModelProperty("公众号支付必传")
    private String openid;

    /**
     * 创建用户充值支付信息
     *
     * @param clientType
     * @param tradeNo
     * @param totalFee
     * @return
     */
    public static PrepayRequest buildUserUp(ClientType clientType, String tradeNo, Long totalFee) {
        return PrepayRequest.builder()
                .orderType(OrderType.USER_UP)
                .clientType(clientType)
                .tradeNo(tradeNo)
                .totalFee(totalFee)
                .subject("ZCXY-在线交易")
                .build();
    }

    /**
     * 创建订单交易支付信息
     *
     * @param clientType
     * @param tradeNo
     * @param totalFee
     * @return
     */
    public static PrepayRequest buildTradeOrder(ClientType clientType, String tradeNo, Long totalFee) {
        return PrepayRequest.builder()
                .orderType(OrderType.TRADE_ORDER)
                .clientType(clientType)
                .tradeNo(tradeNo)
                .totalFee(totalFee)
                .subject("ZCXY-在线交易")
                .build();
    }


    /**
     * 创建专业交易订单信息
     *
     * @param clientType
     * @param tradeNo
     * @param totalFee
     * @return
     */
    public static PrepayRequest buildTransfer(ClientType clientType, String tradeNo, Long totalFee) {
        return PrepayRequest.builder()
                .orderType(OrderType.TRANSFER)
                .clientType(clientType)
                .tradeNo(tradeNo)
                .totalFee(totalFee)
                .subject("ZCXY-在线交易")
                .build();
    }

    public enum OrderType {
        /**
         * 用户充值
         */
        USER_UP,

        /**
         * 交易订单
         */
        TRADE_ORDER,

        /**
         * 转账业务
         */
        TRANSFER;
    }
}
