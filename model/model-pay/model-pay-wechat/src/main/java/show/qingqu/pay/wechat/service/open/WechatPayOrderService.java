package show.qingqu.pay.wechat.service.open;

import show.qingqu.pay.wechat.domain.enums.PayOrderStatus;
import show.qingqu.pay.wechat.domain.model.WechatPayOrder;
import show.qingqu.common.mysql.service.BaseService;
import show.qingqu.pay.wechat.domain.request.PrepayRequest;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author kam
 * @since 2018-08-30
 */
public interface WechatPayOrderService extends BaseService<WechatPayOrder> {

    /**
     * 微信支付下单
     *
     * @param request
     * @return
     */
    Map<String, Object> prepay(PrepayRequest request);

    /**
     * 微信支付异步回调
     *
     * @param parameterMap 请求参数
     * @param is           请求流
     * @return success/fail
     */
    String callback(Map<String, String[]> parameterMap, InputStream is);

    /**
     * 根据订单信息异步通知
     *
     * @param payOrder
     */
    boolean syncCallbackNotice(WechatPayOrder payOrder);

    /**
     * 根据内部订单号查询
     *
     * @param outTradeNo
     * @return
     */
    WechatPayOrder findByOutTradeNo(String outTradeNo);

    /**
     * 根据内部订单号和微信单号ID查询微信订单信息
     *
     * @param transactionId
     * @param outTradeNo
     * @return
     */
    Map<String, Object> queryWithWechat(String transactionId, String outTradeNo);

    /**
     * 关闭订单微信
     *
     * @param outTradeNo
     * @return
     */
    void closeWithWechat(String transactionId, String outTradeNo);

    /**
     * 根据订单状态查询
     *
     * @param status
     * @return
     */
    List<WechatPayOrder> findByStatus(PayOrderStatus status);

    /**
     * 新增
     *
     * @param wechatPayOrder
     */
    boolean add(WechatPayOrder wechatPayOrder);

    /**
     * 修改
     *
     * @param wechatPayOrder
     * @return
     */
    boolean update(WechatPayOrder wechatPayOrder);


}
