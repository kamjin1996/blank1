package show.qingqu.pay.wechat.feign.order.bean;

import show.qingqu.pay.wechat.domain.enums.PayType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author kam

 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("支付回调请求包装类")
public class PayCallbackRequest implements Serializable {

    @ApiModelProperty("订单号")
    private String tradeNo;

    @ApiModelProperty("内部单号(第三方支付平台)")
    private String outTradeNo;

    @ApiModelProperty("订单金额")
    private Long totalFee;

    @ApiModelProperty("订单支付方式")
    private PayType payType;
}
