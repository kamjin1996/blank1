package show.qingqu.pay.wechat.service.open.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.egzosn.pay.common.api.PayService;
import com.egzosn.pay.common.bean.RefundOrder;
import show.qingqu.common.exception.ExceptionFactory;
import show.qingqu.pay.wechat.domain.model.WechatPayOrder;
import show.qingqu.pay.wechat.domain.model.WechatPayOrderRefund;
import show.qingqu.pay.wechat.domain.request.RefundRequest;
import show.qingqu.pay.wechat.mapper.WechatPayOrderRefundMapper;
import show.qingqu.pay.wechat.service.open.WechatPayOrderRefundService;
import show.qingqu.common.mysql.service.BaseServiceImpl;
import show.qingqu.pay.wechat.service.open.WechatPayOrderService;
import show.qingqu.pay.wechat.utils.CommonUtils;
import show.qingqu.pay.wechat.utils.TradeNoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 微信支付退款 服务实现类
 * </p>
 *
 * @author kam
 * @since 2019-01-10
 */
@Slf4j
@Service
public class WechatPayOrderRefundServiceImpl extends BaseServiceImpl<WechatPayOrderRefundMapper, WechatPayOrderRefund> implements WechatPayOrderRefundService {

    public static final String RESULT_CODE = "result_code";

    public static final String RETURN_CODE = "return_code";

    public static final String REFUND_ID = "refund_id";

    @Autowired
    private PayService payService;

    @Autowired
    private WechatPayOrderService wechatPayOrderService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public synchronized void refund(RefundRequest request) {
        WechatPayOrder wechatPayOrder = this.wechatPayOrderService.findByOutTradeNo(request.getOutTradeNo());

        String refundNo = TradeNoUtil.tradeNo(); // 退款单号
        RefundOrder refundOrder = new RefundOrder(
                refundNo,
                wechatPayOrder.getTransactionId(),
                wechatPayOrder.getOutTradeNo(),
                new BigDecimal(CommonUtils.fen2yuan(request.getRefundFee())),
                new BigDecimal(CommonUtils.fen2yuan(wechatPayOrder.getTotalFee())));
        log.info("\n【微信退款】开始：{}", JSON.toJSONString(refundOrder));
        Map<String, Object> map = this.payService.refund(refundOrder);

        String resultCode = (String) map.get(RESULT_CODE);
        String returnCode = (String) map.get(RETURN_CODE);
        if ("SUCCESS".equals(returnCode) && "SUCCESS".equals(resultCode)) {
            log.info("\n【微信退款】成功：{}", JSON.toJSONString(map));
            String refundId = (String) map.get(REFUND_ID);
            this.add(WechatPayOrderRefund.builder()
                    .refundFee(request.getRefundFee())
                    .refundId(refundId)
                    .outRefundNo(refundNo)
                    .outTradeNo(wechatPayOrder.getOutTradeNo())
                    .transactionId(wechatPayOrder.getTransactionId())
                    .totalFee(wechatPayOrder.getTotalFee())
                    .build());
        } else {
            String errCode = (String) map.get("err_code");
            String errCodeDes = (String) map.get("err_code_des");
            log.error("\n【微信退款】失败：code:{} err_code:{} err_code_des:{}", resultCode, errCode, errCodeDes);
            throw ExceptionFactory.build("微信退款失败：" + errCodeDes);
        }
    }

    @Override
    public long sumTotalRefundFeeByOutTradeNo(String outTradeNo) {
        return this.baseMapper.sumTotalRefundFeeByOutTradeNo(outTradeNo);
    }

    @Override
    public List<WechatPayOrderRefund> findByOutTradeNo(String outTradeNo) {
        QueryWrapper<WechatPayOrderRefund> wrapper = new QueryWrapper<>();
        wrapper.eq("out_trade_no", outTradeNo);
        return this.list(wrapper);
    }

    @Override
    public WechatPayOrderRefund findById(Long id) {
        return this.getById(id);
    }

    @Override
    public boolean add(WechatPayOrderRefund wechatPayOrderRefund) {
        return this.save(wechatPayOrderRefund);
    }

    @Override
    public boolean update(WechatPayOrderRefund wechatPayOrderRefund) {
        return this.updateById(wechatPayOrderRefund);
    }
}
