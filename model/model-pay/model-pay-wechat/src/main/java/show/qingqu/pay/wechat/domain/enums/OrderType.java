package show.qingqu.pay.wechat.domain.enums;

import lombok.Getter;

/**
 * @author kam

 * 对于平台的收入支出，
 * 用户的支付，退款->平台收入，
 * 用户提现->平台支出
 **/
@Getter
public enum OrderType {
    /**
     * 收入
     */
    INCOME(0),

    /***
     * 支出
     */
    PAY(1);

    private int type;

    OrderType(int type) {
        this.type = type;
    }
}
