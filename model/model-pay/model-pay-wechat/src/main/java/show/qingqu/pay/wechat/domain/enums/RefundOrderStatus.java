package show.qingqu.pay.wechat.domain.enums;

import lombok.Getter;

/**
 * @author kam

 * 订单退款状态
 **/
@Getter
public enum RefundOrderStatus {

    /**
     * 订单初始化
     */
    INIT(0),

    /**
     * 退款成功
     */
    SUCCESS(1),

    /**
     * 退款关闭
     */
    REFUNDCLOSE(2),

    /**
     * 退款处理中
     */
    PROCESSING(3),

    /**
     * 退款异常，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败
     */
    CHANGE(4);

    private int status;

    RefundOrderStatus(int status) {
        this.status = status;
    }
}
