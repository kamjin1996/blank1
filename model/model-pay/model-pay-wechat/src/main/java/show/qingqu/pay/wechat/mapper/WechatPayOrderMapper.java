package show.qingqu.pay.wechat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import show.qingqu.pay.wechat.domain.model.WechatPayOrder;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author kam
 * @since 2018-08-30
 */
public interface WechatPayOrderMapper extends BaseMapper<WechatPayOrder> {

}
