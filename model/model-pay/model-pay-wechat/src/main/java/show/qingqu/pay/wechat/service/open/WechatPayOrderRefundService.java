package show.qingqu.pay.wechat.service.open;


import show.qingqu.pay.wechat.domain.model.WechatPayOrderRefund;
import show.qingqu.common.mysql.service.BaseService;
import show.qingqu.pay.wechat.domain.request.RefundRequest;

import java.util.List;

/**
 * <p>
 * 微信支付退款 服务类
 * </p>
 *
 * @author kam
 * @since 2019-01-10
 */
public interface WechatPayOrderRefundService extends BaseService<WechatPayOrderRefund> {

    /**
     * 订单退款
     *
     * @param request
     */
    void refund(RefundRequest request);

    /**
     * 根据微信内部单号查询退款总额
     *
     * @param outTradeNo
     * @return
     */
    long sumTotalRefundFeeByOutTradeNo(String outTradeNo);

    /**
     * 根据内部订单号查询
     *
     * @param outTradeNo
     * @return
     */
    List<WechatPayOrderRefund> findByOutTradeNo(String outTradeNo);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    WechatPayOrderRefund findById(Long id);


    /**
     * 新增
     *
     * @param wechatPayOrderRefund
     */
    boolean add(WechatPayOrderRefund wechatPayOrderRefund);

    /**
     * 修改
     *
     * @param wechatPayOrderRefund
     * @return
     */
    boolean update(WechatPayOrderRefund wechatPayOrderRefund);


}
