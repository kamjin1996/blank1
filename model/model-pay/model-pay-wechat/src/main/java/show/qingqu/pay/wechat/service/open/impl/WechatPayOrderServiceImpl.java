package show.qingqu.pay.wechat.service.open.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.egzosn.pay.common.api.PayService;
import com.egzosn.pay.common.bean.PayOrder;
import com.egzosn.pay.common.bean.result.PayError;
import com.egzosn.pay.common.exception.PayErrorException;
import com.egzosn.pay.wx.bean.WxTransactionType;
import show.qingqu.common.exception.ExceptionFactory;
import show.qingqu.common.utils.FeignUtil;
import show.qingqu.pay.wechat.domain.enums.ClientType;
import show.qingqu.pay.wechat.domain.enums.PayOrderStatus;
import show.qingqu.pay.wechat.domain.enums.PayType;
import show.qingqu.pay.wechat.domain.model.WechatPayOrder;
import show.qingqu.pay.wechat.domain.request.PrepayRequest;
import show.qingqu.pay.wechat.feign.order.FeignBurseBalancePrepayService;
import show.qingqu.pay.wechat.feign.order.FeignOrderCustomService;
import show.qingqu.pay.wechat.feign.order.FeignTransferService;
import show.qingqu.pay.wechat.feign.order.bean.PayCallbackRequest;
import show.qingqu.pay.wechat.mapper.WechatPayOrderMapper;
import show.qingqu.pay.wechat.service.open.WechatPayOrderService;
import show.qingqu.common.mysql.service.BaseServiceImpl;
import show.qingqu.pay.wechat.utils.CommonUtils;
import show.qingqu.pay.wechat.utils.TradeNoUtil;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author kam
 * @since 2018-08-30
 */
@Slf4j
@Service
public class WechatPayOrderServiceImpl extends BaseServiceImpl<WechatPayOrderMapper, WechatPayOrder> implements WechatPayOrderService {

    public static final String OUT_TRADE_NO = "out_trade_no";

    public static final String TRANSACTION_ID = "transaction_id";

    public static final String RESULT_CODE = "result_code";

    public static final String RETURN_CODE = "return_code";

    @Autowired
    private FeignOrderCustomService feignOrderCustomService;

    @Autowired
    private FeignBurseBalancePrepayService feignBurseBalancePrepayService;

    @Autowired
    private FeignTransferService feignTransferService;

    @Autowired
    private PayService payService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public synchronized Map<String, Object> prepay(PrepayRequest request) {
        String outTradeNo = TradeNoUtil.tradeNo(); // 微信内部订单号
        PayOrder payOrder = new PayOrder();
        payOrder.setOutTradeNo(outTradeNo);
        payOrder.setPrice(new BigDecimal(CommonUtils.fen2yuan(request.getTotalFee())));
        payOrder.setSubject(request.getSubject());
        // 判断支付类型
        if (ClientType.APP.equals(request.getClientType())) {
            payOrder.setTransactionType(WxTransactionType.APP);
        } else if (ClientType.PC.equals(request.getClientType())) {
            payOrder.setTransactionType(WxTransactionType.NATIVE);
        }

        Map<String, Object> map;
        try {
            log.info("\n【微信支付】开始下单：{}", payOrder.toString());
            map = this.payService.orderInfo(payOrder);
            log.info("\n【微信支付】下单成功：{}", JSON.toJSONString(map));
        } catch (PayErrorException ex) {
            PayError payError = ex.getPayError();
            log.error("\n【微信支付】异常错误： code:{} \nmsg:{} \nmessage:{}", payError.getErrorCode(), payError.getErrorMsg(), payError.getString());
            throw ExceptionFactory.build("微信支付下单错误");
        }
        this.add(WechatPayOrder.builder()
                .orderType(request.getOrderType())
                .clientType(request.getClientType())
                .totalFee(request.getTotalFee())
                .outTradeNo(outTradeNo)
                .tradeNo(request.getTradeNo())
                .subject(request.getSubject())
                .openid(request.getOpenid())
                .status(PayOrderStatus.INIT)
                .request(JSON.toJSONString(map))
                .build());
        return map;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String callback(Map<String, String[]> parameterMap, InputStream is) {
        Map<String, Object> params = this.payService.getParameter2Map(parameterMap, is);
        log.info("\n【微信支付回调】数据：{}", JSON.toJSONString(params));

        if (CollectionUtils.isEmpty(params)) {
            log.error("\n【微信支付回调】支付信息回调数据为空");
            return this.payService.getPayOutMessage("FAIL", "NO").toMessage();
        }
        if (!this.payService.verify(params)) {
            log.error("\n【微信支付回调】校检不通过 \n【微信支付回调】数据：{}", JSON.toJSONString(params));
            return this.payService.getPayOutMessage("FAIL", "NO").toMessage();
        }

        String resultCode = (String) params.get(RESULT_CODE);
        if (!"SUCCESS".equals(resultCode)) {
            String errCode = (String) params.get("err_code");
            String errCodeDes = (String) params.get("err_code_des");
            log.error("\n【微信支付回调】支付结果错误，业务结果:{}，错误代码：{}，错误代码描述：{}", resultCode, errCode, errCodeDes);
            return this.payService.getPayOutMessage("FAIL", "NO").toMessage();
        }

        String outTradeNo = (String) params.get(OUT_TRADE_NO);
        String transactionId = (String) params.get(TRANSACTION_ID);

        // 保存订单信息
        WechatPayOrder payOrder = this.findByOutTradeNo(outTradeNo);
        payOrder.setTransactionId(transactionId);
        payOrder.setStatus(PayOrderStatus.SUCCESS);
        payOrder.setResponse(JSON.toJSONString(params));
        payOrder.setOkTime(DateTime.now().toDate());
        this.update(payOrder);

        // 通知回调支付结果
        if (this.syncCallbackNotice(payOrder)) {
            return this.payService.getPayOutMessage("SUCCESS", "OK").toMessage();
        } else {
            return this.payService.getPayOutMessage("FAIL", "NO").toMessage();
        }
    }


    /**
     * 支付成功异步通知业务服务
     *
     * @param payOrder
     */
    @Override
    public boolean syncCallbackNotice(WechatPayOrder payOrder) {
        PrepayRequest.OrderType orderType = payOrder.getOrderType();

        PayCallbackRequest callbackRequest = PayCallbackRequest.builder()
                .outTradeNo(payOrder.getOutTradeNo())
                .totalFee(payOrder.getTotalFee())
                .tradeNo(payOrder.getTradeNo())
                .payType(PayType.WECHAT)
                .build();
        try {
            if (PrepayRequest.OrderType.TRADE_ORDER.equals(orderType)) {
                FeignUtil.handle(this.feignOrderCustomService.orderPayCallback(callbackRequest));
            } else if (PrepayRequest.OrderType.USER_UP.equals(orderType)) {
                FeignUtil.handle(this.feignBurseBalancePrepayService.balancePayCallback(callbackRequest));
            } else if (PrepayRequest.OrderType.TRANSFER.equals(orderType)) {
                FeignUtil.handle(this.feignTransferService.transferPayCallback(callbackRequest));
            } else {
                throw new RuntimeException("\n【支付宝回调】业务服务处理失败,无效的业务类型");
            }
            // 修改订单回调状态
            payOrder.setCallback(Boolean.TRUE);
            this.update(payOrder);
            return true;
        } catch (Exception ex) {
            log.error("\n【微信支付回调】业务服务处理失败 \n【微信支付回调】订单信息:{}", JSON.toJSONString(callbackRequest), ex);
            return false;
        }
    }

    @Override
    public WechatPayOrder findByOutTradeNo(String outTradeNo) {
        QueryWrapper<WechatPayOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("out_trade_no", outTradeNo);
        return this.getOne(wrapper);
    }

    @Override
    public Map<String, Object> queryWithWechat(String transactionId, String outTradeNo) {
        Map map = this.payService.query(transactionId, outTradeNo);
        String resultCode = (String) map.get(RESULT_CODE);
        if (!"SUCCESS".equals(resultCode)) {
            String errCode = (String) map.get("err_code");
            String errCodeDes = (String) map.get("err_code_des");
            log.error("\n【微信支付查询结果】查询失败 \n【微信支付查询结果】业务结果:{}\n【微信支付查询结果】错误代码：{}\n【微信支付查询结果】错误代码描述：{}", resultCode, errCode, errCodeDes);
            throw ExceptionFactory.build("根据单号查询失败");
        }
        return map;
    }

    @Override
    public void closeWithWechat(String transactionId, String outTradeNo) {
        Map map = this.payService.close(transactionId, outTradeNo);
        String resultCode = (String) map.get(RESULT_CODE);
        String returnCode = (String) map.get(RETURN_CODE);
        if (!("SUCCESS".equals(returnCode) && "SUCCESS".equals(resultCode))) {
            String errCode = (String) map.get("err_code");
            String errCodeDes = (String) map.get("err_code_des");
            log.error("\n【微信支付】根据单号查询失败，业务结果:{}，错误代码：{}，错误代码描述：{}", resultCode, errCode, errCodeDes);
            throw ExceptionFactory.build("根据单号查询失败");
        }
    }

    @Override
    public List<WechatPayOrder> findByStatus(PayOrderStatus status) {
        QueryWrapper<WechatPayOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("status", status);
        return this.list(wrapper);
    }

    @Override
    public boolean add(WechatPayOrder wechatPayOrder) {
        return this.save(wechatPayOrder);
    }

    @Override
    public boolean update(WechatPayOrder wechatPayOrder) {
        return this.updateById(wechatPayOrder);
    }
}
