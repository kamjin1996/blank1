package show.qingqu.pay.wechat.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 订单号生成
 */
public final class TradeNoUtil {

    /**
     * 生成唯一订单号
     *
     * @return
     */
    public static String tradeNo() {
        return String.valueOf(IdWorker.nextId());
    }
}
