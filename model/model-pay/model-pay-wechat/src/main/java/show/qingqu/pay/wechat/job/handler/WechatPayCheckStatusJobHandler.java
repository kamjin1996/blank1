package show.qingqu.pay.wechat.job.handler;

import com.alibaba.fastjson.JSON;
import show.qingqu.pay.wechat.domain.enums.PayOrderStatus;
import show.qingqu.pay.wechat.domain.model.WechatPayOrder;
import show.qingqu.pay.wechat.service.open.WechatPayOrderService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * 微信支付订单状态，超时订单关闭，未通知业务订单发送通知 5 分钟一次
 */
@Slf4j
@Component
@JobHandler(value = "WechatPayCheckStatusJobHandler")
public class WechatPayCheckStatusJobHandler extends IJobHandler {

    public static final String TRADE_STATE = "trade_state";

    public static final String TRANSACTION_ID = "transaction_id";

    @Autowired
    private WechatPayOrderService wechatPayOrderService;

    /**
     * 定时检查微信支付订单状态
     *
     * @param param
     * @return
     */
    public ReturnT<String> execute(String param) {
        List<WechatPayOrder> orderList = this.wechatPayOrderService.findByStatus(PayOrderStatus.INIT);
        orderList.stream().forEach(order -> {
            Map<String, Object> map = this.wechatPayOrderService.queryWithWechat(order.getTransactionId(), order.getOutTradeNo());
            PayOrderStatus payOrderStatus = PayOrderStatus.valueOf((String) map.get(TRADE_STATE));
            String transactionId = (String) map.get(TRANSACTION_ID);

            // 保存订单信息
            order.setTransactionId(transactionId);
            order.setStatus(payOrderStatus);
            order.setResponse(JSON.toJSONString(map));
            order.setOkTime(DateTime.now().toDate());

            // 回调通知业务服务
            if (PayOrderStatus.SUCCESS.equals(payOrderStatus)) {
                if (!order.getCallback()) {
                    if (this.wechatPayOrderService.syncCallbackNotice(order)) {
                        order.setCallback(Boolean.TRUE);
                    }
                }
            } else {
                // 订单超时关闭
                if (order.timeOutPay()) {
                    this.wechatPayOrderService.closeWithWechat(order.getTransactionId(), order.getOutTradeNo());
                    order.setStatus(PayOrderStatus.CLOSED);
                }
            }
            this.wechatPayOrderService.update(order);
        });
        String msg = String.format("检查支付订单状态成功,处理状态信息，本次执行共处理%s条数据", orderList.size());
        return new ReturnT(200, msg);
    }
}
