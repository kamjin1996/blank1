package show.qingqu.pay.wechat.domain.enums;

/**
 * @author kam
 *
 **/
public interface BaseConst {

    String SUCCESS = "SUCCESS";

    String RETURN_MSG = "return_msg";

    String RETURN_CODE = "return_code";

}
