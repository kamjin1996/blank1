package show.qingqu.pay.wechat.domain.query;

import show.qingqu.pay.wechat.domain.model.WechatPayOrder;
import show.qingqu.common.mysql.page.PageHelp;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 * 分页查询
 * </p>
 *
 * @author kam
 * @since 2018-08-30
 */
@Data
@ApiModel("分页查询")
public class WechatPayOrderQuery extends PageHelp<WechatPayOrder> {

    private static final long serialVersionUID = 1L;


}
