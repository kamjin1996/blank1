package show.qingqu.pay.wechat.job.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import show.qingqu.pay.wechat.domain.enums.PayOrderStatus;
import show.qingqu.pay.wechat.domain.model.WechatPayOrder;
import show.qingqu.pay.wechat.service.open.WechatPayOrderService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 检查支付成功未回调订单，回调至业务服务器 2 分钟一次
 */
@Slf4j
@Component
@JobHandler(value = "WechatPayCallbackJobHandler")
public class WechatPayCallbackJobHandler extends IJobHandler {

    @Autowired
    private WechatPayOrderService wechatPayOrderService;

    /**
     * 定时检查微信支付订单状态
     *
     * @param param
     * @return
     */
    public ReturnT<String> execute(String param) {
        QueryWrapper<WechatPayOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("status", PayOrderStatus.SUCCESS)
                .eq("callback", Boolean.FALSE);
        List<WechatPayOrder> orderList = this.wechatPayOrderService.list(wrapper);
        orderList.stream().forEach(order -> {
            if (this.wechatPayOrderService.syncCallbackNotice(order)) {
                order.setCallback(Boolean.TRUE);
                this.wechatPayOrderService.update(order);
            }
        });
        String msg = String.format("检查支付成功未回调业务订单，本次执行共处理%s条数据", orderList.size());
        return new ReturnT(200, msg);
    }
}
