package show.qingqu.pay.wechat.domain.enums;

import lombok.Getter;

/**
 * @author kam

 * 交易类型(指微信交易平台)
 **/
@Getter
public enum TradeType {

    /**
     * 公众号支付
     */
    JSAPI(0),

    /**
     * 原生扫码支付
     */
    NATIVE(1),

    /**
     * app支付
     */
    APP(2);


    private int type;

    TradeType(int type) {
        this.type = type;
    }
}
