package show.qingqu.pay.wechat.feign.order;

import show.qingqu.common.result.Result;
import show.qingqu.pay.wechat.feign.order.bean.PayCallbackRequest;
import show.qingqu.pay.wechat.feign.order.impl.FeignBurseBalancePrepayServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * 微信支付服务调用
 */
@FeignClient(value = "order", fallback = FeignBurseBalancePrepayServiceImpl.class)
public interface FeignBurseBalancePrepayService {

    /**
     * 订单支付成功回调通知
     *
     * @return 订单支付成功回调
     */
    @PostMapping(value = "/feign/burse/balance/prepay/callback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<Object> balancePayCallback(@RequestBody PayCallbackRequest request);


}