package show.qingqu.pay.wechat.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Expand;

import java.io.*;
import java.util.Date;

/**
 * @author kam

 **/
public class FileUtils {

    /**
     * 创建文件夹
     *
     * @param filePath 文件路径
     * @param dir      文件夹
     */
    private static String mkdir(String filePath, String dir) {
        String fileName = DateUtils.date2Str(new Date(), "yyyyMM");
        File file = new File(filePath + fileName);
        if (!file.exists() && !file.isDirectory()) {
            file.mkdir();
        }
        String dayPath = filePath + fileName + "\\" + "\\" + dir;
        File dirs = new File(filePath + fileName + "\\" + "\\" + dir);
        if (!dirs.exists() && !dirs.isDirectory()) {
            dirs.mkdir();
        }
        return dayPath + "\\" + "\\";
    }

    /**
     * 写入文件
     *
     * @param inputStream inputStream
     * @param dir         文件路径
     * @return true/false
     */
    public static String writeFile(InputStream inputStream, String dir) {
        String dayPath;
        String fileName;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            fileName = DateUtils.date2Str(new Date(), "yyyy-MM-dd");
            dayPath = mkdir(dir, fileName);
            String completePath = dayPath + fileName + ".zip";
            OutputStream outputStream = new FileOutputStream(completePath);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
            byte[] bytes = new byte[1024];
            int len;
            while ((len = bufferedInputStream.read(bytes, 0, bytes.length)) != -1) {
                bufferedOutputStream.write(bytes, 0, len);
            }
            inputStream.close();
            bufferedInputStream.close();
            bufferedOutputStream.close();
            unzip(dayPath, fileName + ".zip");
        } catch (Exception e) {
            e.printStackTrace();
            return StringUtils.EMPTY;
        }
        return dayPath;
    }

    /**
     * 筛选文件
     *
     * @param files 待筛选文件
     * @return 文件
     */
    public static File filterFile(File[] files) {
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.getName().endsWith("账务明细.csv")) {
                    return file;
                }
            }
        }
        return null;
    }

    private static void unzip(String filePath, String fileName) {
        try {
            File file = new File(filePath);
            if (!file.exists() && (file.length() <= 0)) {
                throw new IllegalArgumentException("要解压的文件不存在");
            }
            Project project = new Project();
            Expand expand = new Expand();
            expand.setProject(project);
            expand.setTaskType("unzip");
            expand.setTaskName("unzip");
            expand.setEncoding("GBK");
            expand.setSrc(new File(filePath + fileName));
            expand.setDest(new File(filePath));
            expand.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        mkdir("D:\\", "2018-08-22");
    }

}
