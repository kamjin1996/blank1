package show.qingqu.pay.wechat.domain.query;

import show.qingqu.pay.wechat.domain.model.WechatPayOrderRefund;
import show.qingqu.common.mysql.page.PageHelp;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 * 微信支付退款分页查询
 * </p>
 *
 * @author kam
 * @since 2019-01-10
 */
@Data
@ApiModel("微信支付退款分页查询")
public class WechatPayOrderRefundQuery extends PageHelp<WechatPayOrderRefund> {

    private static final long serialVersionUID = 1L;


}
