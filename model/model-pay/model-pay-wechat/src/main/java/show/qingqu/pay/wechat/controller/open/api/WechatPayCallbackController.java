package show.qingqu.pay.wechat.controller.open.api;

import show.qingqu.common.result.Result;
import show.qingqu.pay.wechat.domain.request.PrepayRequest;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import show.qingqu.pay.wechat.service.open.WechatPayOrderService;

import show.qingqu.common.controller.BaseController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author kam
 * @since 2018-08-30
 */
@Slf4j
@Controller
@RequestMapping("/wechat/pay")
@Api(value = "WechatPayCallbackController", tags = "【微信支付回调控制器】")
public class WechatPayCallbackController extends BaseController {

    @Autowired
    private WechatPayOrderService wechatPayOrderService;

    /**
     * 微信支付下单
     *
     * @return 微信支付下单
     */
    @ResponseBody
    @PostMapping(value = "/prepay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "微信支付下单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Object> prepay(@RequestBody PrepayRequest request) {
        return response(this.wechatPayOrderService.prepay(request));
    }

    /**
     * 微信支付异步回调接口
     *
     * @param request request
     * @throws Exception Exception
     */
    @PostMapping(value = "/async/callback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "微信支付异步回调接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void callback(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String callback = this.wechatPayOrderService.callback(request.getParameterMap(), request.getInputStream());
        log.info("【微信支付回调】返回数据:{}", callback);
        response.getWriter().write(callback);
    }
}

