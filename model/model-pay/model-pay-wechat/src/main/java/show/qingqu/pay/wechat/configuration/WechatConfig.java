package show.qingqu.pay.wechat.configuration;

import com.egzosn.pay.common.api.PayService;
import com.egzosn.pay.common.http.HttpConfigStorage;
import com.egzosn.pay.common.util.sign.SignUtils;
import com.egzosn.pay.wx.api.WxPayConfigStorage;
import com.egzosn.pay.wx.api.WxPayService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * @author kam

 **/
@Component
public class WechatConfig {

    @Value("${pay.wechat.appId}")
    private String appId;

    @Value("${pay.wechat.mchId}")
    private String mchId;

    @Value("${pay.wechat.notifyUrl}")
    private String notifyUrl;

    @Value("${pay.wechat.returnUrl}")
    private String returnUrl;

    @Value("${pay.wechat.keyStore}")
    private String keyStore;

    @Value("${pay.wechat.storePassword}")
    private String storePassword;

    @Value("${pay.wechat.secretKey}")
    private String secretKey;

    @Value("${pay.wechat.keyPublic}")
    private String keyPublic;

    @Bean
    public PayService wechatPayService() {
        WxPayConfigStorage wxPayConfigStorage = new WxPayConfigStorage();
        wxPayConfigStorage.setMchId(mchId);
        wxPayConfigStorage.setAppid(appId);

        wxPayConfigStorage.setKeyPublic(keyPublic);
        wxPayConfigStorage.setSecretKey(secretKey);
        wxPayConfigStorage.setNotifyUrl(notifyUrl);
        wxPayConfigStorage.setReturnUrl(returnUrl);
        wxPayConfigStorage.setSignType(SignUtils.MD5.name());
        wxPayConfigStorage.setInputCharset("utf-8");

        PayService payService = new WxPayService(wxPayConfigStorage);
        HttpConfigStorage httpConfigStorage = new HttpConfigStorage();

        httpConfigStorage.setKeystore(keyStore);
        httpConfigStorage.setStorePassword(storePassword);
        httpConfigStorage.setPath(true);

        //最大连接数
        httpConfigStorage.setMaxTotal(20);
        //默认的每个路由的最大连接数
        httpConfigStorage.setDefaultMaxPerRoute(10);
        payService.setRequestTemplateConfigStorage(httpConfigStorage);
        return payService;
    }

}
