package show.qingqu.pay.wechat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import show.qingqu.pay.wechat.domain.model.WechatPayOrderRefund;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.repository.query.Param;

/**
 * <p>
 * 微信支付退款 Mapper 接口
 * </p>
 *
 * @author kam
 * @since 2019-01-10
 */
public interface WechatPayOrderRefundMapper extends BaseMapper<WechatPayOrderRefund> {

    @Select("SELECT IFNULL(sum(refund_fee) ,0) FROM wechat_pay_order_refund WHERE out_trade_no = #{outTradeNo}")
    long sumTotalRefundFeeByOutTradeNo(@Param("outTradeNo") String outTradeNo);

}
