package show.qingqu.pay.wechat.domain.enums;

import lombok.Getter;

/**
 * @author kam

 * 提现订单状态(提现到微信零钱，提现到银行卡)
 **/
@Getter
public enum WithdrawOrderStatus {

    /**
     * 订单初始化
     */
    INIT(0),

    /**
     * 付款成功
     */
    SUCCESS(1),

    /**
     * 处理中
     */
    PROCESSING(2),

    /**
     * 付款失败,需要替换付款单号重新发起付款
     */
    FAILED(3),

    /**
     * 银行退票，订单状态由付款成功流转至退票,退票时付款金额和手续费会自动退还
     */
    BANK_FAIL(4);

    private int status;

    WithdrawOrderStatus(int status) {
        this.status = status;
    }
}
