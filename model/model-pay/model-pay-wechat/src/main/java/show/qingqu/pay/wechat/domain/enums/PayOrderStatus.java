package show.qingqu.pay.wechat.domain.enums;

import lombok.Getter;

/**
 * @author kam

 * 订单支付状态
 **/
@Getter
public enum PayOrderStatus {

    /**
     * 订单初始化
     */
    INIT(0),

    /**
     * 支付成功
     */
    SUCCESS(1),

    /**
     * 转入退款
     */
    REFUND(2),

    /**
     * 未支付
     */
    NOTPAY(3),

    /**
     * 已关闭
     */
    CLOSED(4),

    /**
     * 已撤销（刷卡支付）
     */
    REVOKED(5),

    /**
     * 用户支付中
     */
    USERPAYING(6),

    /**
     * 支付失败(其他原因，如银行返回失败)
     */
    PAYERROR(7);

    private int status;

    PayOrderStatus(int status) {
        this.status = status;
    }
}
