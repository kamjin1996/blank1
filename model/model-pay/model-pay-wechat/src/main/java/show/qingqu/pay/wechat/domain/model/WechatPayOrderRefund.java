package show.qingqu.pay.wechat.domain.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import show.qingqu.common.mysql.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 微信支付退款
 * </p>
 *
 * @author kam
 * @since 2019-01-10
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("微信支付退款")
public class WechatPayOrderRefund extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 内部订单号
     */
    @TableField("out_trade_no")
    @ApiModelProperty("内部订单号")
    private String outTradeNo;
    /**
     * 微信支付订单号
     */
    @TableField("transaction_id")
    @ApiModelProperty("微信支付订单号")
    private String transactionId;

    /**
     * 内部退款单号
     */
    @TableField("out_refund_no")
    @ApiModelProperty("内部退款单号")
    private String outRefundNo;
    /**
     * 微信退款单号
     */
    @TableField("refund_id")
    @ApiModelProperty("微信退款单号")
    private String refundId;
    /**
     * 退款金额
     */
    @TableField("refund_fee")
    @ApiModelProperty("退款金额")
    private Long refundFee;
    /**
     * 订单总金额
     */
    @TableField("total_fee")
    @ApiModelProperty("订单总金额")
    private Long totalFee;
    /**
     * 退款时间
     */
    @TableField("create_time")
    @ApiModelProperty("退款时间")
    private Date createTime;


}
