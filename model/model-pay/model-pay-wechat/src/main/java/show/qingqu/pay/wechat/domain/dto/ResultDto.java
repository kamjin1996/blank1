package show.qingqu.pay.wechat.domain.dto;

import lombok.Data;

/**
 * @author kam

 **/
@Data
public class ResultDto {

    private boolean success;

    private String msg;

    public ResultDto() {
    }

    public ResultDto(boolean success) {
        this.success = success;
    }

}
