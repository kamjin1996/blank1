package show.qingqu.pay.alipay.domain.enums;

import lombok.Getter;

/**
 * @author kam
 **/
@Getter
public enum TransferStatus {

    /**
     * 转账 等待处理
     */
    INIT(0),

    /**
     * 转账 成功（配合"单笔转账到银行账户接口"产品使用时, 同一笔单据多次查询有可能从成功变成退票状态）
     */
    SUCCESS(1),

    /**
     * 转账 处理中
     */
    DEALING(2),

    /**
     * 转账 失败
     */
    FAIL(3),

    /**
     * 转账 退票（仅配合"单笔转账到银行账户接口"产品使用时会涉及, 具体退票原因请参见fail_reason返回值）
     */
    REFUND(4),

    /**
     * 转账 状态未知
     */
    UNKNOWN(5);

    private int status;

    TransferStatus(int status) {
        this.status = status;
    }

    public static int getStatusByName(String name) {
        TransferStatus transferOrderStatus = TransferStatus.valueOf(name);
        return transferOrderStatus.getStatus();
    }
}
