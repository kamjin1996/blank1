package show.qingqu.pay.alipay.mapper;

import show.qingqu.common.mysql.mapper.BaseMapper;
import show.qingqu.pay.alipay.domain.model.AlipayTransferOrder;

/**
 * <p>
 * 转账至用户 Mapper 接口
 * </p>
 *
 * @author kam
 * @since 2019-01-14
 */
public interface AlipayTransferOrderMapper extends BaseMapper<AlipayTransferOrder> {

}
