package show.qingqu.pay.alipay.domain.enums;

import lombok.Getter;

/**
 * @author kam
 **/
@Getter
public enum OrderDetailType {

    /**
     * 支付
     */
    PAYMENT(0),

    /**
     * 退款
     */
    REFUND(1),

    /**
     * 转账
     */
    TRANSFER(2);

    private int type;

    OrderDetailType(int type) {
        this.type = type;
    }
}
