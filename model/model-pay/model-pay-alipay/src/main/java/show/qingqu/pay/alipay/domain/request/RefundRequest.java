package show.qingqu.pay.alipay.domain.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author kam
 **/
@Data
@ApiModel("订单退款")
public class RefundRequest implements Serializable {

    @ApiModelProperty("订单号")
    private String outTradeNo;

    @ApiModelProperty("退款金额")
    private Long refundFee;
}
