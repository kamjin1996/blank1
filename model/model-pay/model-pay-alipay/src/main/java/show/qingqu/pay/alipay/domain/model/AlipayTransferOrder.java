package show.qingqu.pay.alipay.domain.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import show.qingqu.common.mysql.model.BaseModel;
import show.qingqu.pay.alipay.domain.enums.DrawAlipayType;
import show.qingqu.pay.alipay.domain.enums.TransferStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 转账至用户
 * </p>
 *
 * @author kam
 * @since 2019-01-14
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("转账至用户")
public class AlipayTransferOrder extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 内部订单号
     */
    @TableField("out_biz_no")
    @ApiModelProperty("内部订单号")
    private String outBizNo;

    /**
     * 支付宝系统中的交易流水号
     */
    @TableField("order_id")
    @ApiModelProperty("支付宝系统中的交易流水号")
    private String orderId;

    /**
     * 总金额
     */
    @TableField("amount")
    @ApiModelProperty("总金额")
    private Long amount;

    /**
     * 收款方账户
     */
    @TableField("payee_account")
    @ApiModelProperty("收款方账户")
    private String payeeAccount;

    /**
     * 用户名
     */
    @TableField("payee_real_name")
    @ApiModelProperty("用户名")
    private String payeeRealName;

    /**
     * 账户类型
     */
    @TableField("payee_type")
    @ApiModelProperty("账户类型")
    private DrawAlipayType payeeType;

    /**
     * 提现时间
     */
    @TableField("create_time")
    @ApiModelProperty("提现时间")
    private Date createTime;

    /**
     * 预计到账时间
     */
    @TableField("arrival_time_end")
    @ApiModelProperty("预计到账时间")
    private String arrivalTimeEnd;

    /**
     * 成功时间
     */
    @TableField("pay_date")
    @ApiModelProperty("成功时间")
    private String payDate;

    /**
     * 状态
     */
    @TableField("status")
    @ApiModelProperty("状态")
    private TransferStatus status;

    /**
     * 失败原因
     */
    @TableField("fail_msg")
    @ApiModelProperty("失败原因")
    private String failMsg;

    /**
     * 请求参数
     */
    @TableField("request")
    @ApiModelProperty("请求参数")
    private String request;

    /**
     * 返回参数
     */
    @TableField("response")
    @ApiModelProperty("返回参数")
    private String response;


}
