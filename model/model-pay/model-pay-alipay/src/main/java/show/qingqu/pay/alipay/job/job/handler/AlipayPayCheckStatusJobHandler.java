package show.qingqu.pay.alipay.job.job.handler;

import com.alibaba.fastjson.JSON;
import show.qingqu.pay.alipay.domain.enums.PayOrderStatus;
import show.qingqu.pay.alipay.domain.model.AlipayPayOrder;
import show.qingqu.pay.alipay.service.open.AlipayPayOrderService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 微信支付订单状态，超时订单关闭，未通知业务订单发送通知 5 分钟一次
 */
@Slf4j
@Component
@JobHandler(value = "AlipayPayCheckStatusJobHandler")
public class AlipayPayCheckStatusJobHandler extends IJobHandler {

    @Autowired
    private AlipayPayOrderService alipayPayOrderService;

    public static final String TRADE_STATUS = "trade_status";

    public static final String TRADE_NO = "trade_no";

    /**
     * 定时检查微信支付订单状态
     *
     * @param param
     * @return
     */
    public ReturnT<String> execute(String param) {
        List<AlipayPayOrder> orderList = this.alipayPayOrderService.findByStatus(PayOrderStatus.INIT);
        orderList.addAll(this.alipayPayOrderService.findByStatus(PayOrderStatus.WAIT_BUYER_PAY));
        orderList.stream().forEach(order -> {
            Map<String, Object> map;
            try {
                map = this.alipayPayOrderService.queryWithAlipay(order.getAlipayTradeNo(), order.getOutTradeNo());
            } catch (Exception e) {
                //忽略支付宝订单未查询到异常，继续执行任务
                return;
            }

            PayOrderStatus payOrderStatus = PayOrderStatus.valueOf((String) map.get(TRADE_STATUS));
            String alipayTradeNo = (String) map.get(TRADE_NO);

            // 保存订单信息
            order.setAlipayTradeNo(alipayTradeNo);
            order.setStatus(payOrderStatus);
            order.setResponse(JSON.toJSONString(map));
            order.setOkTime(DateTime.now().toDate());

            // 回调通知业务服务
            if (PayOrderStatus.TRADE_SUCCESS.equals(payOrderStatus)) {
                if (!order.getCallback()) {
                    if (this.alipayPayOrderService.callbackNotice(order)) {
                        order.setCallback(Boolean.TRUE);
                    }
                }
            } else {
                // 订单超时关闭
                if (order.timeOutPay()) {
                    try {
                        this.alipayPayOrderService.closeWithAliPay(order.getAlipayTradeNo(), order.getOutTradeNo());
                    } catch (Exception e) {
                        //忽略关闭失败错误
                        return;
                    }
                    order.setStatus(PayOrderStatus.TRADE_CLOSED);
                }
            }
            this.alipayPayOrderService.update(order);
        });
        String msg = String.format("检查支付订单状态成功,处理状态信息，本次执行共处理%s条数据", orderList.size());
        return new ReturnT(200, msg);
    }
}
