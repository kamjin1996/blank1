package show.qingqu.pay.alipay.domain.enums;

import lombok.Getter;

/**
 * @author kam
 **/
@Getter
public enum ResponseCode {

    /**
     * 接口调用成功
     */
    SUCCESS("10000"),

    /**
     * 服务不可用
     */
    ERROR("20000"),

    /**
     * 授权权限不足(应用权限)
     */
    AUTH_LESS("20001"),

    /**
     * 缺少必选参数
     */
    MISSING("40001"),

    /**
     * 非法参数
     */
    ILLEGAL_PARAMETER("40002"),

    /**
     * 业务处理失败
     */
    FAIL("40004"),

    /**
     * 权限不足(接口权限)
     */
    PERMISSION_ERROR("40006");


    ResponseCode(String code) {
        this.code = code;
    }

    private String code;

}
