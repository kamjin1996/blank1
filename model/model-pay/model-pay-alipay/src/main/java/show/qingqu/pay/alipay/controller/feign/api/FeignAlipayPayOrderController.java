package show.qingqu.pay.alipay.controller.feign.api;

import show.qingqu.common.controller.BaseController;
import show.qingqu.common.result.Result;
import show.qingqu.pay.alipay.domain.enums.PayOrderStatus;
import show.qingqu.pay.alipay.domain.model.AlipayPayOrder;
import show.qingqu.pay.alipay.domain.model.AlipayTransferOrder;
import show.qingqu.pay.alipay.domain.request.PrepayRequest;
import show.qingqu.pay.alipay.domain.request.RefundRequest;
import show.qingqu.pay.alipay.domain.request.TransferRequest;
import show.qingqu.pay.alipay.service.open.AlipayPayOrderService;
import show.qingqu.pay.alipay.service.open.AlipayRefundOrderService;
import show.qingqu.pay.alipay.service.open.AlipayTransferOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author kam
 * @since 2018-08-30
 */
@RestController
@RequestMapping("/feign/alipay/pay")
@Api(value = "FeignWechatPayOrderController", tags = "【支付宝订单服务调用】")
public class FeignAlipayPayOrderController extends BaseController {

    @Autowired
    private AlipayPayOrderService alipayPayOrderService;

    @Autowired
    private AlipayRefundOrderService alipayRefundOrderService;

    @Autowired
    private AlipayTransferOrderService alipayTransferOrderService;

    /**
     * 支付宝支付下单
     *
     * @return 支付宝支付下单
     */
    @PostMapping(value = "/prepay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "支付宝支付下单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Object> prepay(@RequestBody PrepayRequest request) {
        return response(this.alipayPayOrderService.prepay(request));
    }

    /**
     * 支付宝订单退款
     *
     * @return
     */
    @PostMapping(value = "/refund", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "支付宝订单退款", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Object> refund(@RequestBody RefundRequest request) {
        AlipayPayOrder payOrder = this.alipayPayOrderService.findByOutTradeNo(request.getOutTradeNo());
        if (Objects.isNull(payOrder)) {
            return responseMsg("订单不存在");
        }
        if (!payOrder.getStatus().equals(PayOrderStatus.TRADE_SUCCESS)) {
            return responseMsg("订单未支付无法申请退款");
        }
        long totalRefundFee = this.alipayRefundOrderService.sumTotalRefundFeeByOutTradeNo(payOrder.getOutTradeNo());
        long surRefundFee = payOrder.getTotalFee() - totalRefundFee;
        if (surRefundFee < request.getRefundFee()) {
            return responseMsg("订单剩余可退款金额不足");
        }
        this.alipayRefundOrderService.refund(request);
        return response();
    }

    /**
     * 支付宝提现
     *
     * @return
     */
    @PostMapping(value = "/transfer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "支付宝提现", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<AlipayTransferOrder> transfer(@RequestBody TransferRequest request) {
        return response(this.alipayTransferOrderService.transfer(request));
    }
}