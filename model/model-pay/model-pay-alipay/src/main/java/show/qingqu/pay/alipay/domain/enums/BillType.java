package show.qingqu.pay.alipay.domain.enums;

/**
 * @author kam
 **/
public enum BillType {

    /**
     * 商户基于支付宝交易收单的业务账单
     */
    TRADE,

    /**
     * 商户支付宝余额收入及支出等资金变动的帐务账单
     */
    SIGNCUSTOMER;

}
