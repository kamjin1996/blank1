package show.qingqu.pay.alipay.domain.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author kam
 **/
@Data
@ApiModel("转账到支付宝账户")
public class TransferRequest implements Serializable {

    private static final long serialVersionUID = 229468427907208726L;

    @NotNull(message = "转账单号不能为空")
    @ApiModelProperty("转账单号")
    private String outBizNo;

    @NotNull(message = "转账金额不能为空")
    @ApiModelProperty("转账金额(分)")
    @Min(value = 10, message = "提现金额至少为0.10元")
    private Long amount;

    @NotBlank(message = "收款方账户不能为空")
    @ApiModelProperty("收款方账户")
    private String payeeAccount;

    @ApiModelProperty("收款方真实姓名")
    private String payeeRealName;
}
