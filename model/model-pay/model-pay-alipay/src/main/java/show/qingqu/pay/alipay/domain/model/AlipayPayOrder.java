package show.qingqu.pay.alipay.domain.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import show.qingqu.common.mysql.model.BaseModel;
import show.qingqu.pay.alipay.domain.enums.ClientType;
import show.qingqu.pay.alipay.domain.enums.PayOrderStatus;
import show.qingqu.pay.alipay.domain.request.PrepayRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 *
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("支付宝支付订单信息")
public class AlipayPayOrder extends BaseModel {

    private static final long serialVersionUID = 8113475743675684445L;

    @TableField("trade_no")
    @ApiModelProperty("订单号,支付成功回调使用")
    private String tradeNo;

    @TableField("order_type")
    @ApiModelProperty("订单类型,支付成功回调使用")
    private PrepayRequest.OrderType orderType;

    @TableField("alipay_trade_no")
    @ApiModelProperty("支付宝支付订单号")
    private String alipayTradeNo;

    @TableField("out_trade_no")
    @ApiModelProperty("内部订单号")
    private String outTradeNo;

    @TableField("status")
    @ApiModelProperty("订单状态")
    private PayOrderStatus status;

    @TableField("total_fee")
    @ApiModelProperty("订单金额")
    private Long totalFee;

    @TableField("subject")
    @ApiModelProperty("订单标题")
    private String subject;

    @TableField("client_type")
    @ApiModelProperty("支付类型,多种付款渠道")
    private ClientType clientType;

    @TableField("buyer_id")
    @ApiModelProperty("买家支付宝用户号")
    private String BuyerId;

    @TableField("buyer_logon_id")
    @ApiModelProperty("买家支付宝账号")
    private String buyerLogonId;

    @TableField("callback")
    @ApiModelProperty("是否已回调业务服务器处理")
    private Boolean callback;

    @TableField("request")
    @ApiModelProperty("下单请求参数JSON")
    private String request;

    @TableField("response")
    @ApiModelProperty("查询返回参数JSON")
    private String response;

    /**
     * 订单创建时间
     */
    @TableField("create_time")
    @ApiModelProperty("订单创建时间")
    private Date createTime;

    /**
     * 订单取消时间
     */
    @TableField("cancel_time")
    @ApiModelProperty("订单取消时间")
    private Date cancelTime;

    /**
     * 订单支付成功时间
     */
    @TableField("ok_time")
    @ApiModelProperty("订单支付成功时间")
    private Date okTime;

    public boolean timeOutPay() {
        long dif = 1000 * 60 * 30; // 30 分钟
        long create = createTime.getTime();
        long now = System.currentTimeMillis();
        return (now - create) > dif;
    }
}
