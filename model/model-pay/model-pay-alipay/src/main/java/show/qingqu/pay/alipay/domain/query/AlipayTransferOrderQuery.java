package show.qingqu.pay.alipay.domain.query;

import show.qingqu.pay.alipay.domain.model.AlipayTransferOrder;
import show.qingqu.common.mysql.page.PageHelp;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 * 转账至用户分页查询
 * </p>
 *
 * @author kam
 * @since 2019-01-14
 */
@Data
@ApiModel("转账至用户分页查询")
public class AlipayTransferOrderQuery extends PageHelp<AlipayTransferOrder> {

    private static final long serialVersionUID = 1L;


}
