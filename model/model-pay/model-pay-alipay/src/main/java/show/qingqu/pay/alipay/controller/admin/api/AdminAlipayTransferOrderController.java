package show.qingqu.pay.alipay.controller.admin.api;

import show.qingqu.common.controller.BaseController;
import show.qingqu.common.result.Result;
import show.qingqu.pay.alipay.domain.model.AlipayTransferOrder;
import show.qingqu.pay.alipay.domain.request.TransferRequest;
import show.qingqu.pay.alipay.service.open.AlipayTransferOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 转账至用户 前端控制器
 * </p>
 *
 * @author kam
 * @since 2019-01-14
 */
@RestController
@RequestMapping("/admin/alipay/transfer/order")
@Api(value = "AdminAlipayTransferOrderController", tags = "【转账管理】")
public class AdminAlipayTransferOrderController extends BaseController {

    @Autowired
    private AlipayTransferOrderService alipayTransferOrderService;

    /**
     * 支付宝提现
     *
     * @return
     */
    @PostMapping(value = "/transfer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "支付宝提现", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<AlipayTransferOrder> transfer(@RequestBody TransferRequest request) {
        return response(this.alipayTransferOrderService.transfer(request));
    }
}

