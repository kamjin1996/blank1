package show.qingqu.pay.alipay.mapper;

import show.qingqu.common.mysql.mapper.BaseMapper;
import show.qingqu.pay.alipay.domain.model.AlipayPayOrder;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
public interface AlipayPayOrderMapper extends BaseMapper<AlipayPayOrder> {

}
