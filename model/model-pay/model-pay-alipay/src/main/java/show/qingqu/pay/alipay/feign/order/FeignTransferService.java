package show.qingqu.pay.alipay.feign.order;

import show.qingqu.common.result.Result;
import show.qingqu.pay.alipay.feign.order.bean.PayCallbackRequest;
import show.qingqu.pay.alipay.feign.order.impl.FeignTransferServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * 微信支付服务调用
 */
@FeignClient(value = "order", fallback = FeignTransferServiceImpl.class)
public interface FeignTransferService {
    /**
     * 订单支付成功回调通知
     *
     * @return 订单支付成功回调
     */
    @PostMapping(value = "/feign/transfer/callback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Result<Object> transferPayCallback(@RequestBody PayCallbackRequest request);
}