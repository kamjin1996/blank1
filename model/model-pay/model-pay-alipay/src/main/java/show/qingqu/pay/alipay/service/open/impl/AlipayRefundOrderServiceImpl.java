package show.qingqu.pay.alipay.service.open.impl;

import com.alibaba.fastjson.JSON;
import com.egzosn.pay.common.api.PayService;
import com.egzosn.pay.common.bean.RefundOrder;
import show.qingqu.common.exception.ExceptionFactory;
import show.qingqu.pay.alipay.domain.model.AlipayPayOrder;
import show.qingqu.pay.alipay.domain.model.AlipayRefundOrder;
import show.qingqu.pay.alipay.domain.request.RefundRequest;
import show.qingqu.pay.alipay.mapper.AlipayRefundOrderMapper;
import show.qingqu.pay.alipay.service.open.AlipayPayOrderService;
import show.qingqu.pay.alipay.service.open.AlipayRefundOrderService;
import show.qingqu.common.mysql.service.BaseServiceImpl;
import show.qingqu.pay.alipay.utils.CommonUtils;
import show.qingqu.pay.alipay.utils.TradeNoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
@Slf4j
@Service
public class AlipayRefundOrderServiceImpl extends BaseServiceImpl<AlipayRefundOrderMapper, AlipayRefundOrder> implements AlipayRefundOrderService {

    @Autowired
    private PayService payService;

    @Autowired
    private AlipayPayOrderService payOrderService;

    @Override
    public long sumTotalRefundFeeByOutTradeNo(String outTradeNo) {
        return this.baseMapper.sumTotalRefundFeeByOutTradeNo(outTradeNo);
    }

    @Override
    public synchronized void refund(RefundRequest request) {
        AlipayPayOrder alipayPayOrder = this.payOrderService.findByOutTradeNo(request.getOutTradeNo());
        String refundNo = TradeNoUtil.tradeNo(); // 退款单号
        RefundOrder refundOrder = new RefundOrder(
                refundNo,
                alipayPayOrder.getAlipayTradeNo(),
                alipayPayOrder.getOutTradeNo(),
                new BigDecimal(CommonUtils.fen2yuan(request.getRefundFee())),
                new BigDecimal(CommonUtils.fen2yuan(alipayPayOrder.getTotalFee())));
        log.info("\n【支付宝退款】开始：{}", JSON.toJSONString(refundOrder));

        Map<String, Object> map = this.payService.refund(refundOrder);

        if (CollectionUtils.isEmpty(map)) {
            log.error("\n【支付宝退款】失败返回参数集为空");
            throw ExceptionFactory.build("调用支付宝退款接口失败");
        }
        Map<String, Object> response = CommonUtils.getResponseMap(map, "alipay_trade_refund_response");
        String code = (String) response.get("code");
        String msg = (String) response.get("Success");
        String fundChange = (String) response.get("fund_change");

        if ("10000".equals(code)) {
            if (!"Y".equals(fundChange)) {
                log.error("\n【支付宝退款】失败,退款金额未发生变化：{}", JSON.toJSONString(map));
                throw ExceptionFactory.build("支付宝退款失败，资金为发生变化");
            }
            log.info("\n【支付宝退款】成功：{}", JSON.toJSONString(map));
            String AlipayTradeNo = (String) response.get("trade_no");
            String outTradeNo = (String) response.get("out_trade_no");
            String buyerLogonId = (String) response.get("buyer_logon_id");
            String buyerUserId = (String) response.get("buyer_user_id");
            String refundFee = (String) response.get("refund_fee");
            this.add(AlipayRefundOrder.builder()
                    .refundFee(request.getRefundFee())
                    .outRefundNo(refundNo)
                    .outTradeNo(outTradeNo)
                    .alipayTradeNo(AlipayTradeNo)
                    .totalFee(alipayPayOrder.getTotalFee())
                    .buyerId(buyerUserId)
                    .buyerLogonId(buyerLogonId)
                    .build());
        } else {
            log.error("\n【支付宝退款】失败：code:{} msg:{}", code, msg);
            throw ExceptionFactory.build("支付宝退款失败：" + msg);
        }
    }

    @Override
    public boolean add(AlipayRefundOrder alipayRefundOrder) {
        return this.save(alipayRefundOrder);
    }

    @Override
    public boolean update(AlipayRefundOrder alipayRefundOrder) {
        return this.update(alipayRefundOrder);
    }

}
