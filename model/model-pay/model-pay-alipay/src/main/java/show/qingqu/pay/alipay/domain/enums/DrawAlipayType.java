package show.qingqu.pay.alipay.domain.enums;


/**
 * 支付宝收款方账户类型
 * 1、ALIPAY_USERID：支付宝账号对应的支付宝唯一用户号。以2088开头的16位纯数字组成。
 * 2、ALIPAY_LOGONID：支付宝登录号，支持邮箱和手机号格式。
 */
public enum DrawAlipayType {

    ALIPAY_USERID("用户号"),

    ALIPAY_LOGONID("登录号");

    public String title;

    DrawAlipayType(String title) {
        this.title = title;
    }
}
