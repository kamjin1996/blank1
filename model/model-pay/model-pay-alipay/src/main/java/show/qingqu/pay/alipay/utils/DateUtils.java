package show.qingqu.pay.alipay.utils;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Date;
import java.util.Locale;

/**
 * @author kam
 **/
public class DateUtils {

    private static final String FORMAT = "yyyy-MM-dd";

    /**
     * 字符串转日期
     *
     * @param str 字符串
     * @return 日期
     */
    public static Date str2Date(String str) {
        return DateTime.parse(str, DateTimeFormat.forPattern(FORMAT)).toDate();
    }

    /**
     * 获取某日期的昨天
     *
     * @param date date
     * @return 某日期的昨天
     */
    public static Date yesterday(Date date) {
        DateTime dateTime = new DateTime(date);
        return dateTime.minusDays(1).toDate();
    }

    /**
     * 字符串转日期
     *
     * @param str    字符串
     * @param format 日期格式
     * @return 日期
     */
    public static Date str2Date(String str, String format) {
        if (StringUtils.isBlank(format)) {
            return new Date();
        }
        return DateTime.parse(str, DateTimeFormat.forPattern(format)).toDate();
    }

    /**
     * 日期转字符串格式
     *
     * @param date   日期
     * @param format 格式
     * @return 字符串格式日期
     */
    public static String date2Str(Date date, String format) {
        if (StringUtils.isBlank(format)) {
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(format, Locale.CHINESE);
    }

    /**
     * 31分钟后
     */
    public static Date after31Minutes() {
        DateTime dateTime = new DateTime();
        dateTime.plusMinutes(31);
        return dateTime.toDate();
    }

    public static void main(String[] args) {
        DateTime dateTime = new DateTime();
        System.out.println(dateTime.minusDays(2).toDate());
    }

}
