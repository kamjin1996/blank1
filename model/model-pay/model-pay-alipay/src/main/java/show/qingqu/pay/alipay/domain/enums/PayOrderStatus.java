package show.qingqu.pay.alipay.domain.enums;

import lombok.Getter;

/**
 * @author kam
 **/
@Getter
public enum PayOrderStatus {

    /**
     * 订单初始化
     */
    INIT(0),

    /**
     * 支付(交易创建，等待买家付款)
     */
    WAIT_BUYER_PAY(4),

    /**
     * 支付 未付款交易超时关闭，或支付完成后全额退款
     */
    TRADE_CLOSED(2),

    /**
     * 支付 交易结束，不可退款
     */
    TRADE_FINISHED(3),

    /**
     * 支付 交易支付成功
     */
    TRADE_SUCCESS(1);

    private int status;

    PayOrderStatus(int status) {
        this.status = status;
    }

    public static int getStatusByName(String name) {
        PayOrderStatus payOrderStatus = PayOrderStatus.valueOf(name);
        return payOrderStatus.getStatus();
    }
}
