package show.qingqu.pay.alipay.domain.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author kam
 **/
@Data
@ApiModel("查询接口")
public class QueryRequest implements Serializable {
    private static final long serialVersionUID = -1641563032354277882L;

    @NotNull(message = "订单号不能为空")
    @ApiModelProperty("内部订单号")
    private Long orderId;
}
