package show.qingqu.pay.alipay.domain.query;

import show.qingqu.pay.alipay.domain.model.AlipayRefundOrder;
import show.qingqu.common.mysql.page.PageHelp;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 分页查询
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("分页查询")
public class AlipayRefundOrderQuery extends PageHelp<AlipayRefundOrder> {

    private static final long serialVersionUID = -6967346157696735025L;
}
