package show.qingqu.pay.alipay.domain.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author kam
 **/
@Data
@ApiModel("转账查询接口")
public class TransferQueryRequest implements Serializable {
    private static final long serialVersionUID = 7532236212921271964L;


    @NotNull(message = "订单号不能为空")
    @ApiModelProperty("内部订单号")
    private Long orderId;

    @NotNull(message = "支付宝订单号不能为空")
    @ApiModelProperty("支付宝订单号")
    private String tradeNo;
}
