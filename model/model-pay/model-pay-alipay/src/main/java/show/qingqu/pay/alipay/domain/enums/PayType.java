package show.qingqu.pay.alipay.domain.enums;

/**
 * 支付方式
 */
public enum PayType {

    ALIPAY("支付宝"),

    WECHAT("微信"),

    BALANCE("余额");

    public String title;

    PayType(String title) {
        this.title = title;
    }
}
