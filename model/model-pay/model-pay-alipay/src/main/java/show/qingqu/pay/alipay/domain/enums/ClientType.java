package show.qingqu.pay.alipay.domain.enums;

/**
 * 客户端类型
 */
public enum ClientType {

    APP("APP"),

    PC("PC");

    public String title;

    ClientType(String title) {
        this.title = title;
    }
}
