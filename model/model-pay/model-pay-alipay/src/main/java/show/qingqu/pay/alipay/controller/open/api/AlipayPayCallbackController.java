package show.qingqu.pay.alipay.controller.open.api;

import show.qingqu.common.controller.BaseController;
import show.qingqu.common.result.Result;
import show.qingqu.pay.alipay.domain.request.PrepayRequest;
import show.qingqu.pay.alipay.service.open.AlipayPayOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author kam
 * @since 2018-08-30
 */
@Slf4j
@Controller
@RequestMapping("/alipay/pay")
@Api(value = "AlipayPayCallbackController", tags = "【支付宝支付回调控制器】")
public class AlipayPayCallbackController extends BaseController {

    @Autowired
    private AlipayPayOrderService alipayPayOrderService;

    /**
     * 支付宝支付下单
     *
     * @return 支付宝支付下单
     */
    @PostMapping(value = "/prepay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "支付宝支付下单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success", response = Result.class)
    public Result<Map<String, Object>> prepay(@RequestBody PrepayRequest request) {
        return response(this.alipayPayOrderService.prepay(request));
    }


    /**
     * 支付宝支付异步回调接口
     *
     * @param request request
     * @throws Exception Exception
     */
    @PostMapping(value = "/async/callback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "支付宝支付异步回调接口", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void callback(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String callback = this.alipayPayOrderService.callback(request.getParameterMap(), request.getInputStream());
        log.info("【支付宝支付回调】返回数据:{}", callback);
        response.getWriter().write(callback);
    }
}

