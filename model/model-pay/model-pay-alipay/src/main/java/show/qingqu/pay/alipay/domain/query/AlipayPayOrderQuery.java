package show.qingqu.pay.alipay.domain.query;

import show.qingqu.pay.alipay.domain.model.AlipayPayOrder;
import show.qingqu.common.mysql.page.PageHelp;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 分页查询
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("分页查询")
public class AlipayPayOrderQuery extends PageHelp<AlipayPayOrder> {

    private static final long serialVersionUID = -3452450250064233627L;
}
