package show.qingqu.pay.alipay.service.open;

import show.qingqu.pay.alipay.domain.model.AlipayRefundOrder;
import show.qingqu.common.mysql.service.BaseService;
import show.qingqu.pay.alipay.domain.request.RefundRequest;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
public interface AlipayRefundOrderService extends BaseService<AlipayRefundOrder> {

    /**
     * 根据内部订单号查询退款总金额
     *
     * @param outTradeNo
     * @return
     */
    long sumTotalRefundFeeByOutTradeNo(String outTradeNo);

    /**
     * 退款接口
     *
     * @param request request
     * @return ResultDto
     */
    void refund(RefundRequest request);

    /**
     * 新增
     *
     * @param alipayRefundOrder
     */
    boolean add(AlipayRefundOrder alipayRefundOrder);

    /**
     * 修改
     *
     * @param alipayRefundOrder
     * @return
     */
    boolean update(AlipayRefundOrder alipayRefundOrder);


}
