package show.qingqu.pay.alipay.service.open;


import show.qingqu.common.mysql.service.BaseService;
import show.qingqu.pay.alipay.domain.enums.PayOrderStatus;
import show.qingqu.pay.alipay.domain.model.AlipayPayOrder;
import show.qingqu.pay.alipay.domain.request.PrepayRequest;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
public interface AlipayPayOrderService extends BaseService<AlipayPayOrder> {


    /**
     * 支付宝统一下单
     *
     * @param request
     * @return
     */
    Map<String, Object> prepay(PrepayRequest request);

    /**
     * 验证异步支付宝回调
     *
     * @param parameterMap 请求参数
     * @param is           请求流
     * @return success/fail
     */
    String callback(Map<String, String[]> parameterMap, InputStream is);

    /**
     * 根据订单信息异步通知
     *
     * @param payOrder
     */
    boolean callbackNotice(AlipayPayOrder payOrder);

    /**
     * 新增
     *
     * @param alipayPayOrder
     * @return
     */
    boolean add(AlipayPayOrder alipayPayOrder);

    /**
     * 更新
     *
     * @param alipayPayOrder
     * @return
     */
    boolean update(AlipayPayOrder alipayPayOrder);

    /**
     * 根据内部订单号查询
     *
     * @param outTradeNo
     * @return
     */
    AlipayPayOrder findByOutTradeNo(String outTradeNo);

    /**
     * 根据订单状态查询
     *
     * @param status
     * @return
     */
    List<AlipayPayOrder> findByStatus(PayOrderStatus status);

    /**
     * 查询支付宝订单信息
     *
     * @param alipayTradeNo
     * @param outTradeNo
     * @return
     */
    Map<String, Object> queryWithAlipay(String alipayTradeNo, String outTradeNo);

    /**
     * 关闭订单
     *
     * @param alipayTradeNo
     * @param outTradeNo
     */
    Map<String, Object> closeWithAliPay(String alipayTradeNo, String outTradeNo);
}
