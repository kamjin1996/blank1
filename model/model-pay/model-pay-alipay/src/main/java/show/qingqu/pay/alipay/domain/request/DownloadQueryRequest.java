package show.qingqu.pay.alipay.domain.request;

import show.qingqu.pay.alipay.domain.enums.BillType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author kam
 **/
@Data
@ApiModel("获取账单下载地址")
public class DownloadQueryRequest implements Serializable {


    @NotNull(message = "账单类型不能为空")
    @ApiModelProperty("账单类型：trade指商户基于支付宝交易收单的业务账单；signcustomer是指基于商户支付宝余额收入及支出等资金变动的帐务账单")
    private BillType billType;

    @NotNull(message = "账单日期不能为空")
    @ApiModelProperty("账单日期：不能下载当日账单")
    private String date;


}
