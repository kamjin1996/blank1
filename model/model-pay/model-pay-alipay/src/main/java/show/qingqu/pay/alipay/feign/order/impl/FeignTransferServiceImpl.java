package show.qingqu.pay.alipay.feign.order.impl;

import com.alibaba.fastjson.JSON;
import show.qingqu.common.result.BaseResponse;
import show.qingqu.common.result.Result;
import show.qingqu.pay.alipay.feign.order.FeignTransferService;
import show.qingqu.pay.alipay.feign.order.bean.PayCallbackRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class FeignTransferServiceImpl implements FeignTransferService {

    @Override
    public Result<Object> transferPayCallback(PayCallbackRequest request) {
        log.error("服务名:{},方法:{}, 参数:{} 调用失败", "order", "transferPayCallback", JSON.toJSONString(request));
        return BaseResponse.getSystemErrorResult("服务调用失败");
    }
}
