package show.qingqu.pay.alipay.domain.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import show.qingqu.common.mysql.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 *
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("支付宝退款订单信息")
public class AlipayRefundOrder extends BaseModel {

    private static final long serialVersionUID = -1070404518356315328L;


    /**
     * 内部订单号
     */
    @TableField("out_trade_no")
    @ApiModelProperty("内部订单号")
    private String outTradeNo;
    /**
     * 支付宝订单号
     */
    @TableField("alipay_trade_no")
    @ApiModelProperty("支付宝订单号")
    private String alipayTradeNo;

    /**
     * 内部退款单号
     */
    @TableField("out_refund_no")
    @ApiModelProperty("内部退款单号")
    private String outRefundNo;
    /**
     * 微信退款单号
     */
    @TableField("refund_id")
    @ApiModelProperty("退款单号")
    private String refundId;

    /**
     * 退款人支付宝登录账号
     */
    @TableField("buyer_logon_id")
    @ApiModelProperty("退款人支付宝登录账号")
    private String buyerLogonId;

    /**
     * 退款人支付宝账号
     */
    @TableField("buyer_id")
    @ApiModelProperty("退款人支付宝账号")
    private String buyerId;

    /**
     * 退款金额
     */
    @TableField("refund_fee")
    @ApiModelProperty("退款金额")
    private Long refundFee;
    /**
     * 订单总金额
     */
    @TableField("total_fee")
    @ApiModelProperty("订单总金额")
    private Long totalFee;
    /**
     * 退款时间
     */
    @TableField("create_time")
    @ApiModelProperty("退款时间")
    private Date createTime;

}
