package show.qingqu.pay.alipay.service.admin;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import show.qingqu.common.mysql.service.BaseService;
import show.qingqu.pay.alipay.domain.model.AlipayTransferOrder;
import show.qingqu.pay.alipay.domain.query.AlipayTransferOrderQuery;

/**
 * <p>
 * 转账至用户 服务类
 * </p>
 *
 * @author kam
 * @since 2019-01-14
 */
public interface AdminAlipayTransferOrderService extends BaseService<AlipayTransferOrder> {

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    AlipayTransferOrder findById(Long id);

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    Page<AlipayTransferOrder> findByPage(AlipayTransferOrderQuery query);

    /**
     * 新增
     *
     * @param alipayTransferOrder
     */
    boolean add(AlipayTransferOrder alipayTransferOrder);

    /**
     * 修改
     *
     * @param alipayTransferOrder
     * @return
     */
    boolean update(AlipayTransferOrder alipayTransferOrder);

    /**
     * 根据ID删除
     *
     * @param id
     */
    void delete(Long id);
}
