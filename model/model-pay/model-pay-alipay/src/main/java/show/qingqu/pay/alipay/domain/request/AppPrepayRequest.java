package show.qingqu.pay.alipay.domain.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author kam
 **/
@Data
@ApiModel("生成APP预订单")
public class AppPrepayRequest implements Serializable {

    private static final long serialVersionUID = -3505787508795604364L;

    @NotBlank(message = "商品标题不能为空")
    @ApiModelProperty("商品标题")
    private String subject;

    @NotBlank(message = "订单描述不能为空")
    @ApiModelProperty("订单描述")
    private String body;

    @NotNull(message = "订单金额不能为空")
    @ApiModelProperty(value = "订单金额(分)")
    private Long amount;

    @NotBlank(message = "订单信息快照不能为空")
    @ApiModelProperty("订单信息快照,json串")
    private String orderMsg;

    @NotNull(message = "订单号不能为空")
    @ApiModelProperty("内部订单号")
    private Long orderId;

}
