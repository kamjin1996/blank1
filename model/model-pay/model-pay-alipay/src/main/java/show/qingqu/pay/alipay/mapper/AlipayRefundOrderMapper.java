package show.qingqu.pay.alipay.mapper;

import show.qingqu.common.mysql.mapper.BaseMapper;
import show.qingqu.pay.alipay.domain.model.AlipayRefundOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
@Repository
public interface AlipayRefundOrderMapper extends BaseMapper<AlipayRefundOrder> {

    @Select("SELECT IFNULL(sum(refund_fee) ,0) FROM alipay_refund_order WHERE out_trade_no = #{outTradeNo}")
    long sumTotalRefundFeeByOutTradeNo(@Param("outTradeNo") String outTradeNo);
}
