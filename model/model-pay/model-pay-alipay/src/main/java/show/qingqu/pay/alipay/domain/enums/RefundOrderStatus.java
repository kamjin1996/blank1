package show.qingqu.pay.alipay.domain.enums;

import lombok.Getter;

/**
 * @author kam
 **/
@Getter
public enum RefundOrderStatus {

    /**
     * 初始化
     */
    INIT(0),

    /**
     * 成功
     */
    SUCCESS(1),

    /**
     * 失败
     */
    FAIL(2);


    private int status;

    RefundOrderStatus(int status) {
        this.status = status;
    }
}
