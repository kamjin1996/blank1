package show.qingqu.pay.alipay.utils;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author kam
 **/
public class CommonUtils {

    /**
     * 负号 -
     */
    private static final String MINUS = "-";

    /**
     * 0 元
     */
    private static final String ZERO = "0.00";

    /**
     * 切割字符串长度
     */
    private static final int LENGHT = 2048;

    /**
     * 分转元
     *
     * @param val 分
     * @return 元
     */
    public static Double fen2yuan(Long val) {
        BigDecimal val1 = new BigDecimal(String.valueOf(val));
        BigDecimal val2 = new BigDecimal(100);
        return val1.divide(val2, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 元转分
     *
     * @param val 元
     * @return 分
     */
    public static Long yuan2fen(String val) {
        if (val.startsWith(MINUS)) {
            val = val.replace("-", "");
        }
        if (ZERO.equals(val)) {
            return 0L;
        }
        BigDecimal val1 = new BigDecimal(val);
        BigDecimal val2 = new BigDecimal(100);
        return val1.multiply(val2).longValue();
    }

    /**
     * 元转分
     *
     * @param val 元
     * @return 分
     */
    public static Long yuan2fen(Long val) {
        BigDecimal val1 = new BigDecimal(val);
        BigDecimal val2 = new BigDecimal(100);
        return val1.multiply(val2).longValue();
    }

    /**
     * 切割字符串长度
     *
     * @param str    字符串
     * @param length 长度
     * @return 切割后字符串长度
     */
    public static String split(String str, int length) {
        if (StringUtils.isNotBlank(str)) {
            if (StringUtils.length(str) > length) {
                return str.substring(0, length);
            } else {
                return str;
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * 切割2048字符串长度
     *
     * @param str 字符串
     * @return 切割后字符串长度
     */
    public static String split(String str) {
        if (StringUtils.isNotBlank(str)) {
            if (StringUtils.length(str) > LENGHT) {
                return str.substring(0, LENGHT);
            } else {
                return str;
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * 取一个数的反数
     *
     * @param val val
     * @return val
     */
    public static Long negative(Long val) {
        return ~val + 1;
    }

    /**
     * 获取调用阿里接口返回map
     *
     * @param map map
     * @param key key
     * @return map
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getResponseMap(Map map, String key) {
        return (Map) map.get(key);
    }


    public static void main(String[] args) {
        System.out.println(~-10 + 1);
    }

}
