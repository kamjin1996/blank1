package show.qingqu.pay.alipay.service.open.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.egzosn.pay.ali.bean.AliTransactionType;
import com.egzosn.pay.common.bean.PayOrder;
import com.egzosn.pay.common.bean.result.PayError;
import com.egzosn.pay.common.exception.PayErrorException;
import com.egzosn.pay.common.http.UriVariables;
import show.qingqu.common.exception.ExceptionFactory;
import show.qingqu.common.utils.FeignUtil;
import show.qingqu.pay.alipay.configuration.AliPayProviderService;
import show.qingqu.pay.alipay.domain.enums.*;
import show.qingqu.pay.alipay.domain.model.AlipayPayOrder;
import show.qingqu.pay.alipay.domain.request.PrepayRequest;
import show.qingqu.pay.alipay.feign.order.FeignBurseBalancePrepayService;
import show.qingqu.pay.alipay.feign.order.FeignOrderCustomService;
import show.qingqu.pay.alipay.feign.order.FeignTransferService;
import show.qingqu.pay.alipay.feign.order.bean.PayCallbackRequest;
import show.qingqu.pay.alipay.mapper.AlipayPayOrderMapper;
import show.qingqu.pay.alipay.service.open.AlipayPayOrderService;
import show.qingqu.common.mysql.service.BaseServiceImpl;
import show.qingqu.pay.alipay.utils.CommonUtils;
import show.qingqu.pay.alipay.utils.TradeNoUtil;
import lombok.extern.slf4j.Slf4j;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author kam
 * @since 2018-08-23
 */
@Slf4j
@Service
public class AlipayPayOrderServiceImpl extends BaseServiceImpl<AlipayPayOrderMapper, AlipayPayOrder> implements AlipayPayOrderService {

    @Autowired
    private AliPayProviderService payService;

    @Autowired
    private FeignOrderCustomService feignOrderCustomService;

    @Autowired
    private FeignBurseBalancePrepayService feignBurseBalancePrepayService;
    @Autowired
    private FeignTransferService feignTransferService;

    @Override
    public Map<String, Object> prepay(PrepayRequest request) {
        String outTradeNo = TradeNoUtil.tradeNo(); // 内部订单号
        PayOrder payOrder = new PayOrder();
        payOrder.setOutTradeNo(outTradeNo);
        payOrder.setPrice(new BigDecimal(CommonUtils.fen2yuan(request.getTotalFee())));
        payOrder.setSubject(request.getSubject());
        payOrder.setBody(request.getSubject());
        Map<String, Object> map = null;
        try {
            // 判断支付类型
            log.info("\n【支付宝支付】开始下单：{}", payOrder.toString());
            if (ClientType.APP.equals(request.getClientType())) {
                payOrder.setTransactionType(AliTransactionType.APP);
                map = this.payService.orderInfo(payOrder);
                String result = UriVariables.getMapToParameters(map);
                map.put("param", result);

            } else if (ClientType.PC.equals(request.getClientType())) {
                map = this.payService.genQrPayMap(payOrder);
            }
            log.info("\n【支付宝支付】下单成功：{}", JSON.toJSONString(map));
        } catch (PayErrorException ex) {
            PayError payError = ex.getPayError();
            log.error("\n【支付宝支付】异常错误： code:{} \nmsg:{} \nmessage:{}", payError.getErrorCode(), payError.getErrorMsg(), payError.getString());
            throw ExceptionFactory.build("支付宝下单错误");
        }
        this.add(AlipayPayOrder.builder()
                .orderType(request.getOrderType())
                .clientType(request.getClientType())
                .totalFee(request.getTotalFee())
                .outTradeNo(outTradeNo)
                .tradeNo(request.getTradeNo())
                .subject(request.getSubject())
                .status(PayOrderStatus.INIT)
                .request(JSON.toJSONString(map))
                .build());
        return map;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public synchronized String callback(Map<String, String[]> parameterMap, InputStream is) {
        Map<String, Object> params = this.payService.getParameter2Map(parameterMap, is);
        log.info("\n【支付宝回调】数据：{}", JSON.toJSONString(params));
        if (CollectionUtils.isEmpty(params)) {
            log.error("\n【支付宝回调】支付信息回调数据为空");
            return this.payService.getPayOutMessage("FAIL", "NO").toMessage();
        }
        if (!this.payService.verify(params)) {
            log.error("\n【支付宝回调】校检不通过。数据：{}", JSON.toJSONString(params));
            return this.payService.getPayOutMessage("FAIL", "NO").toMessage();
        }

        String outTradeNo = (String) params.get("out_trade_no");
        String alipayTradeNo = (String) params.get("trade_no");
        String tradeStatus = (String) params.get("trade_status");
        String buyerId = (String) params.get("buyer_id");
        String buyerLogonId = (String) params.get("buyer_logon_id");

        AlipayPayOrder payOrder = this.findByOutTradeNo(outTradeNo);
        PayOrderStatus payOrderStatus = PayOrderStatus.valueOf(tradeStatus);
        payOrder.setStatus(payOrderStatus);
        payOrder.setBuyerId(buyerId);
        payOrder.setBuyerLogonId(buyerLogonId);
        payOrder.setAlipayTradeNo(alipayTradeNo);
        payOrder.setResponse(JSON.toJSONString(params));
        payOrder.setOkTime(DateTime.now().toDate());
        this.update(payOrder);

        if (this.callbackNotice(payOrder)) {
            return this.payService.getPayOutMessage("success", "成功").toMessage();
        } else {
            return this.payService.getPayOutMessage("FAIL", "NO").toMessage();
        }
    }


    /**
     * 支付成功异步通知业务服务
     *
     * @param payOrder
     */
    @Override
    public boolean callbackNotice(AlipayPayOrder payOrder) {
        PrepayRequest.OrderType orderType = payOrder.getOrderType();
        PayCallbackRequest payCallbackRequest = PayCallbackRequest.builder()
                .outTradeNo(payOrder.getOutTradeNo())
                .totalFee(payOrder.getTotalFee())
                .tradeNo(payOrder.getTradeNo())
                .payType(PayType.ALIPAY)
                .build();
        try {
            if (PrepayRequest.OrderType.TRADE_ORDER.equals(orderType)) {
                FeignUtil.handle(this.feignOrderCustomService.orderPayCallback(payCallbackRequest));
            } else if (PrepayRequest.OrderType.USER_UP.equals(orderType)) {
                FeignUtil.handle(this.feignBurseBalancePrepayService.balancePayCallback(payCallbackRequest));
            } else if (PrepayRequest.OrderType.TRANSFER.equals(orderType)) {
                FeignUtil.handle(this.feignTransferService.transferPayCallback(payCallbackRequest));
            } else {
                throw new RuntimeException("\n【支付宝回调】业务服务处理失败,无效的业务类型");
            }
            // 修改订单回调状态
            payOrder.setCallback(Boolean.TRUE);
            this.update(payOrder);
            return true;
        } catch (Exception ex) {
            log.error("\n【支付宝回调】业务服务处理失败", ex);
            return false;
        }
    }

    @Override
    public boolean add(AlipayPayOrder alipayPayOrder) {
        return this.save(alipayPayOrder);
    }

    @Override
    public boolean update(AlipayPayOrder alipayPayOrder) {
        return this.updateById(alipayPayOrder);
    }

    @Override
    public AlipayPayOrder findByOutTradeNo(String outTradeNo) {
        QueryWrapper<AlipayPayOrder> wrapper = new   QueryWrapper<>();
        wrapper.eq("out_trade_no", outTradeNo);
        return this.getOne(wrapper);
    }

    @Override
    public List<AlipayPayOrder> findByStatus(PayOrderStatus status) {
        QueryWrapper<AlipayPayOrder> wrapper = new  QueryWrapper<>();
        wrapper.eq("status", status);
        return this.list(wrapper);
    }

    @Override
    public Map<String, Object> queryWithAlipay(String alipayTradeNo, String outTradeNo) {
        log.info("\n【支付宝支付】订单查询 alipayTradeNo:{} outTradeNo:{}", alipayTradeNo, outTradeNo);
        Map<String, Object> resultMap = this.payService.query(alipayTradeNo, outTradeNo);
        Map<String, Object> response = CommonUtils.getResponseMap(resultMap, "alipay_trade_query_response");

        String code = (String) response.get("code");
        if (!"10000".equals(code)) {
            log.error("\n【支付宝支付】订单查询失败 \n【支付宝支付】返回信息：{}", JSON.toJSONString(resultMap));
            throw new RuntimeException("支付宝订单查询出错");
        }
        return response;
    }

    @Override
    public Map<String, Object> closeWithAliPay(String alipayTradeNo, String outTradeNo) {
        log.info("\n【支付宝支付】订单关闭 \n【支付宝支付】alipayTradeNo:{} \n【支付宝支付】outTradeNo:{}", alipayTradeNo, outTradeNo);
        Map<String, Object> resultMap = this.payService.close(alipayTradeNo, outTradeNo);
        Map<String, Object> response = CommonUtils.getResponseMap(resultMap, "alipay_trade_close_response");
        String code = (String) response.get("code");
        if (!"10000".equals(code)) {
            log.error("\n【支付宝支付】订单关闭执行失败 \n【支付宝支付】返回信息：{}", JSON.toJSONString(resultMap));
            throw new RuntimeException("支付宝订单关闭出错");
        }
        return response;
    }
}
