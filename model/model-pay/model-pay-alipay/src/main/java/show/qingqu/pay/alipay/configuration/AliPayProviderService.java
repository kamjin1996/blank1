package show.qingqu.pay.alipay.configuration;

import com.alibaba.fastjson.JSONObject;
import com.egzosn.pay.ali.api.AliPayConfigStorage;
import com.egzosn.pay.ali.api.AliPayService;
import com.egzosn.pay.ali.bean.AliTransactionType;
import com.egzosn.pay.common.bean.PayOrder;
import com.egzosn.pay.common.bean.result.PayException;
import com.egzosn.pay.common.exception.PayErrorException;
import com.egzosn.pay.common.http.HttpConfigStorage;
import com.egzosn.pay.common.http.UriVariables;

import java.util.HashMap;
import java.util.Map;

/**
 * AliPayProviderService 重写支付宝服务类
 */
public class AliPayProviderService extends AliPayService {

    public AliPayProviderService(AliPayConfigStorage payConfigStorage, HttpConfigStorage configStorage) {
        super(payConfigStorage, configStorage);
    }


    /**
     * 生成二维码支付
     *
     * @param order 发起支付的订单信息
     * @return 返回图片信息，支付时需要的
     */
    public Map<String, Object> genQrPayMap(PayOrder order) {
        order.setTransactionType(AliTransactionType.SWEEPPAY);
        Map<String, Object> orderInfo = orderInfo(order);
        //预订单
        JSONObject result = getHttpRequestTemplate().postForObject(getReqUrl() + "?" + UriVariables.getMapToParameters(orderInfo), null, JSONObject.class);
        JSONObject response = result.getJSONObject("alipay_trade_precreate_response");
        if (SUCCESS_CODE.equals(response.getString(CODE))) {
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("out_trade_no", response.getString("out_trade_no"));
            resultMap.put("qr_code", response.getString("qr_code"));
            return resultMap;
        }
        throw new PayErrorException(new PayException(response.getString(CODE), response.getString("msg"), result.toJSONString()));
    }


}
