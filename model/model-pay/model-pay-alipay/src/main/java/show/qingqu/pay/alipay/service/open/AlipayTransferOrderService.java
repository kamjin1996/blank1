package show.qingqu.pay.alipay.service.open;

import show.qingqu.pay.alipay.domain.model.AlipayTransferOrder;
import show.qingqu.common.mysql.service.BaseService;
import show.qingqu.pay.alipay.domain.request.TransferRequest;

/**
 * <p>
 * 转账至用户 服务类
 * </p>
 *
 * @author kam
 * @since 2019-01-14
 */
public interface AlipayTransferOrderService extends BaseService<AlipayTransferOrder> {

    /**
     * 转账提现
     *
     * @param request
     */
    AlipayTransferOrder transfer(TransferRequest request);

    /**
     * 根据内部订单查询
     *
     * @param outBizNo
     * @return
     */
    AlipayTransferOrder findByOutBizNo(String outBizNo);

    /**
     * 转账查询
     *
     * @param outBizNo
     * @return
     */
    AlipayTransferOrder transferQuery(String outBizNo);


    /**
     * 新增
     *
     * @param alipayTransferOrder
     */
    boolean add(AlipayTransferOrder alipayTransferOrder);

    /**
     * 修改
     *
     * @param alipayTransferOrder
     * @return
     */
    boolean update(AlipayTransferOrder alipayTransferOrder);
}
