package show.qingqu.pay.alipay.service.admin.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import show.qingqu.common.mysql.service.BaseServiceImpl;
import show.qingqu.pay.alipay.domain.model.AlipayTransferOrder;
import show.qingqu.pay.alipay.domain.query.AlipayTransferOrderQuery;
import show.qingqu.pay.alipay.mapper.AlipayTransferOrderMapper;
import show.qingqu.pay.alipay.service.admin.AdminAlipayTransferOrderService;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * <p>
 * 转账至用户 服务实现类
 * </p>
 *
 * @author kam
 * @since 2019-01-14
 */
@Service
public class AdminAlipayTransferOrderServiceImpl extends BaseServiceImpl<AlipayTransferOrderMapper,AlipayTransferOrder> implements AdminAlipayTransferOrderService {

    @Override
    public AlipayTransferOrder findById(Long id) {
        return this.getById(id);
    }

    @Override
    public Page<AlipayTransferOrder> findByPage(AlipayTransferOrderQuery query) {
        QueryWrapper<AlipayTransferOrder> wrapper = new QueryWrapper<>();
        return this.page(query.getPage(), wrapper);
    }

    @Override
    public void delete(Long id) {
        this.removeById(id);
    }

    @Override
    public boolean add(AlipayTransferOrder alipayTransferOrder) {
        return this.save(alipayTransferOrder);
    }

    @Override
    public boolean update(AlipayTransferOrder alipayTransferOrder) {
        return this.updateById(alipayTransferOrder);
    }
}
