package show.qingqu.pay.alipay.domain.enums;

import lombok.Getter;

/**
 * @author kam
 **/
@Getter
public enum OrderSource {

    /**
     * APP
     */
    APP(0),

    /**
     * 网站
     */
    WEB(1);


    OrderSource(int source) {
        this.source = source;
    }

    /**
     * 订单来源
     */
    private int source;
}
