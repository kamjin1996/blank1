package show.qingqu.pay.alipay.job.job.handler;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import show.qingqu.pay.alipay.domain.enums.PayOrderStatus;
import show.qingqu.pay.alipay.domain.model.AlipayPayOrder;
import show.qingqu.pay.alipay.service.open.AlipayPayOrderService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 检查支付成功未回调订单，回调至业务服务器 2 分钟一次
 */
@Slf4j
@Component
@JobHandler(value = "AlipayPayCallbackJobHandler")
public class AlipayPayCallbackJobHandler extends IJobHandler {

    @Autowired
    private AlipayPayOrderService alipayPayOrderService;

    /**
     * 定时检查支付宝支付订单状态
     *
     * @param param
     * @return
     */
    public ReturnT<String> execute(String param) {
        QueryWrapper<AlipayPayOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("status", PayOrderStatus.TRADE_SUCCESS)
                .eq("callback", Boolean.FALSE);
        List<AlipayPayOrder> orderList = this.alipayPayOrderService.list(wrapper);
        orderList.stream().forEach(order -> {
            if (this.alipayPayOrderService.callbackNotice(order)) {
                order.setCallback(Boolean.TRUE);
                this.alipayPayOrderService.update(order);
            }
        });
        String msg = String.format("检查支付成功未回调业务订单，本次执行共处理%s条数据", orderList.size());
        return new ReturnT(200, msg);
    }
}
