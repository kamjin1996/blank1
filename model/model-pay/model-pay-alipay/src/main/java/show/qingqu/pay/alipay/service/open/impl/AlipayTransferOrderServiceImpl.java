package show.qingqu.pay.alipay.service.open.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.egzosn.pay.common.bean.TransferOrder;
import show.qingqu.common.exception.ExceptionFactory;
import show.qingqu.pay.alipay.configuration.AliPayProviderService;
import show.qingqu.pay.alipay.domain.enums.DrawAlipayType;
import show.qingqu.pay.alipay.domain.enums.TransferStatus;
import show.qingqu.pay.alipay.domain.model.AlipayTransferOrder;
import show.qingqu.pay.alipay.domain.request.TransferRequest;
import show.qingqu.pay.alipay.mapper.AlipayTransferOrderMapper;
import show.qingqu.pay.alipay.service.open.AlipayTransferOrderService;
import show.qingqu.common.mysql.service.BaseServiceImpl;
import show.qingqu.pay.alipay.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 转账至用户 服务实现类
 * </p>
 *
 * @author kam
 * @since 2019-01-14
 */
@Slf4j
@Service
public class AlipayTransferOrderServiceImpl extends BaseServiceImpl<AlipayTransferOrderMapper,AlipayTransferOrder> implements AlipayTransferOrderService {

    @Autowired
    private AliPayProviderService payService;

    @Override
    public synchronized AlipayTransferOrder transfer(TransferRequest request) {
        AlipayTransferOrder transferOrder = this.findByOutBizNo(request.getOutBizNo());
        if (Objects.nonNull(transferOrder)) {
            // 失败重新发起,其他状态查询转账结果
            if (TransferStatus.FAIL.equals(transferOrder.getStatus())) {
                AlipayTransferOrder newTransferOrder = this.alipayTransfer(request.getOutBizNo(), request.getAmount(), request.getPayeeAccount(), request.getPayeeRealName());
                newTransferOrder.setId(transferOrder.getId());
                this.update(newTransferOrder);
                return newTransferOrder;
            } else {
                return this.transferQuery(transferOrder.getOutBizNo());
            }
        } else {
            AlipayTransferOrder newTransferOrder = this.alipayTransfer(request.getOutBizNo(), request.getAmount(), request.getPayeeAccount(), request.getPayeeRealName());
            this.add(newTransferOrder);
            return newTransferOrder;
        }
    }

    /**
     * 发起支付宝转账，返回结果集
     *
     * @param outBizNo
     * @param amount
     * @param payeeAccount
     * @param payeeRealName
     * @return
     */
    private AlipayTransferOrder alipayTransfer(String outBizNo, Long amount, String payeeAccount, String payeeRealName) {
        TransferOrder order = new TransferOrder();
        order.setOutNo(outBizNo);
        order.setAmount(new BigDecimal(CommonUtils.fen2yuan(amount)));
        order.setPayeeAccount(payeeAccount);
        order.setPayeeName(payeeRealName);
        order.setRemark("ZCXY-提现");
        Map<String, Object> map = this.payService.transfer(order);
        if (CollectionUtils.isEmpty(map)) {
            throw ExceptionFactory.build("支付宝转账失败，支付宝未返回信息");
        }
        Map<String, Object> response = CommonUtils.getResponseMap(map, "alipay_fund_trans_toaccount_transfer_response");
        String orderId = (String) response.get("order_id");
        String code = (String) response.get("code");
        AlipayTransferOrder newTransferOrder = AlipayTransferOrder.builder()
                .amount(amount)
                .outBizNo(outBizNo)
                .payeeAccount(payeeAccount)
                .payeeRealName(payeeRealName)
                .payeeType(DrawAlipayType.ALIPAY_LOGONID)
                .build();

        if ("10000".equals(code)) {
            newTransferOrder.setStatus(TransferStatus.INIT);
            newTransferOrder.setOrderId(orderId);
        } else {
            String subCode = (String) response.get("sub_code");
            String subMsg = (String) response.get("sub_msg");
            if ("SYSTEM_ERROR".equals(subCode)) {
                // 支付宝系统故障，不能直接判定为失败
                newTransferOrder.setStatus(TransferStatus.INIT);
                newTransferOrder.setOrderId(orderId);
            } else {
                newTransferOrder.setStatus(TransferStatus.FAIL);
                newTransferOrder.setFailMsg(subMsg);
            }
        }
        String reqJson = JSON.toJSONString(order);
        String resJson = JSON.toJSONString(map);
        newTransferOrder.setRequest(reqJson);
        newTransferOrder.setResponse(resJson);
        log.info("\n【支付宝转账】调用成功 \n【支付宝转账】请求信息:{} \n【支付宝转账】返回信息:{} ", reqJson, resJson);
        return newTransferOrder;
    }


    @Override
    public AlipayTransferOrder findByOutBizNo(String outBizNo) {
        QueryWrapper<AlipayTransferOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("out_biz_no", outBizNo);
        return this.getOne(wrapper);
    }

    @Override
    public AlipayTransferOrder transferQuery(String outBizNo) {
        AlipayTransferOrder transferOrder = this.findByOutBizNo(outBizNo);
        Map<String, Object> resultMap = this.payService.transferQuery(transferOrder.getOutBizNo(), transferOrder.getOrderId());
        if (CollectionUtils.isEmpty(resultMap)) {
            throw ExceptionFactory.build("支付宝查询订单接口调用失败");
        }
        Map<String, Object> response = CommonUtils.getResponseMap(resultMap, "alipay_fund_trans_order_query_response");
        String code = (String) response.get("code");
        if ("10000".equals(code)) {
            String status = (String) response.get("status");
            String failReason = (String) response.get("fail_reason");
            String arrivalTimeEnd = (String) response.get("arrival_time_end");
            String orderId = (String) response.get("order_id");
            String payDate = (String) response.get("pay_date");

            transferOrder.setStatus(TransferStatus.valueOf(status));
            transferOrder.setFailMsg(failReason);
            transferOrder.setArrivalTimeEnd(arrivalTimeEnd);
            transferOrder.setOrderId(orderId);
            transferOrder.setPayDate(payDate);
        } else {
            String subMsg = (String) response.get("sub_msg");
            String subCode = (String) response.get("sub_code");
            log.error("\n【支付宝转账查询】调用失败 \n【支付宝转账查询】sub_code:{} \n【支付宝转账查询】sub_msg :{}", outBizNo, subCode, subMsg);
            throw ExceptionFactory.build("支付宝查询订单失败:" + subMsg);
        }
        String resJson = JSON.toJSONString(resultMap);
        transferOrder.setResponse(resJson);
        this.update(transferOrder);
        log.info("\n【支付宝转账查询】调用成功 \n【支付宝转账查询】请求信息:{} \n【支付宝转账查询】 返回信息:{} ", outBizNo, resJson);
        return transferOrder;
    }

    @Override
    public boolean add(AlipayTransferOrder alipayTransferOrder) {
        return this.save(alipayTransferOrder);
    }

    @Override
    public boolean update(AlipayTransferOrder alipayTransferOrder) {
        return this.updateById(alipayTransferOrder);
    }

}
