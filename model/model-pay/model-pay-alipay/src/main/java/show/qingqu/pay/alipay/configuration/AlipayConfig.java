package show.qingqu.pay.alipay.configuration;

import com.egzosn.pay.ali.api.AliPayConfigStorage;
import com.egzosn.pay.common.http.HttpConfigStorage;
import com.egzosn.pay.common.util.sign.SignUtils;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @author kam
 **/
@Getter
@Component
public class AlipayConfig {

    @Value("${pay.alipay.pid}")
    private String pid;

    @Value("${pay.alipay.privateKey}")
    private String privateKey;

    @Value("${pay.alipay.publicKey}")
    private String publicKey;

    @Value("${pay.alipay.notifyUrl}")
    private String notifyUrl;

    @Value("${pay.alipay.returnUrl}")
    private String returnUrl;

    @Value("${pay.alipay.appid}")
    private String appid;

    @Value("${pay.alipay.seller}")
    private String seller;

    @Value("${pay.alipay.billPath}")
    private String billPath;

    @Bean
    public AliPayProviderService alipayProdService() {
        AliPayConfigStorage aliPayConfigStorage = new AliPayConfigStorage();
        aliPayConfigStorage.setPid(pid);
        aliPayConfigStorage.setKeyPrivate(privateKey);
        aliPayConfigStorage.setNotifyUrl(notifyUrl);
        aliPayConfigStorage.setSignType(SignUtils.RSA2.name());
        aliPayConfigStorage.setInputCharset("utf-8");
        aliPayConfigStorage.setTest(false);
        aliPayConfigStorage.setAppId(appid);
        aliPayConfigStorage.setSeller(seller);
        aliPayConfigStorage.setReturnUrl(returnUrl);
        aliPayConfigStorage.setKeyPublic(publicKey);

        HttpConfigStorage httpConfigStorage = new HttpConfigStorage();
        httpConfigStorage.setMaxTotal(20);
        httpConfigStorage.setDefaultMaxPerRoute(10);
        return new AliPayProviderService(aliPayConfigStorage, httpConfigStorage);
    }
}
