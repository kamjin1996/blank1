package show.qingqu.gateway.zuul.configuration;

import show.qingqu.common.configuration.CorsConfig;
import show.qingqu.common.exception.handler.ExceptionHandlerAdvice;
import show.qingqu.common.exception.handler.RestNotFoundController;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author kam
 *
 * @Description: 全局基础配置bean
 */
@Configuration
public class CommonHandlerConfig {

    @Bean
    @ConditionalOnClass(ExceptionHandlerAdvice.class)
    public ExceptionHandlerAdvice exceptionHandlerAdvice() {
        return new ExceptionHandlerAdvice();
    }

    @Bean
    @ConditionalOnClass(RestNotFoundController.class)
    public RestNotFoundController RestNotFoundController() {
        return new RestNotFoundController();
    }

    @Bean
    @ConditionalOnClass(CorsConfig.class)
    public CorsConfig CorsConfig() {
        return new CorsConfig();
    }
}
