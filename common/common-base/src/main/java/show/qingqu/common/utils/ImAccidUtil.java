package show.qingqu.common.utils;


import com.google.common.collect.Maps;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.Objects;

/**
 * 即时通讯accId生成查询
 */
public class ImAccidUtil {

    /**
     * 命名空间（警告 不能修改）
     */
    public static final Long IM_ACCID_PREFIX = 537342114L;

    /**
     * 根据userId 生成accid
     *
     * @param userId
     * @return
     */
    public static String buildAccid(Long userId) {
        Assert.notNull(userId, "userId isNull");
        return String.valueOf(IM_ACCID_PREFIX ^ userId);
    }

    /**
     * 获取userId
     *
     * @param accid
     * @return
     */
    public static Long getUserId(String accid) {
        Assert.hasText(accid, "accid is isBlank");
        return IM_ACCID_PREFIX ^ Long.valueOf(accid);
    }

    public static void main(String[] args) {
        Map<String, Long> resultMap = Maps.newHashMap();
        for (long i = 0; i < IM_ACCID_PREFIX; i++) {
            String result = ImAccidUtil.buildAccid(i);
            if (Objects.nonNull(resultMap.get(result))) {
                throw new RuntimeException("出现重复数据---->" + i);
            }
            resultMap.put(result, i);
            System.out.println(i + "------->" + result);
        }
    }
}
