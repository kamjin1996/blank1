package show.qingqu.common.sms;

/**
 * @author kam
 *
 * @Desc
 */
public enum AliyunSmsTemplateEnum {
    /**
     * 验证码
     */
    VERIF_CODE("verifCode"),

    /**
     * 经销商认证提醒
     */
    SELLER_AUTH_NOTIFICATION("sellerAuthNotification"),

    /**
     * 修理厂认证成功提醒
     */
    REPAIR_FACTORY_AUTH_SUCCESS("repairFactoryAuthSuccess"),

    /**
     * 修理厂认证失败提醒
     */
    REPAIR_FACTORY_AUTH_FAILURE("repairFacotryAuthFailure"),

    /**
     * 隐藏前提醒
     */
    SELLER_SOLD_OUT_BEFORE("sellerSoldOutBefore"),

    /**
     * 隐藏时提醒
     */
    SELLER_SOLD_OUT_MOMENT("sellerSoldOutMoment"),

    /**
     * 推广APP
     */
    GENERALIZE_APP("generalizeAPP"),

    /**
     * 上线前提醒
     */
    LAUNCH_BEFORE_NOTIFICATION("launchBeforeNotification"),

    /**
     * 财务提现提醒
     */

    FINANCIAL_DRAW_REMINDER("financialDrawReminder"),

    /**
     * 买家完成付款发送短信给卖家
     */
    PAY_SUCCESS_SELLER_NOTIFACATION("paySuccessSellerNotifacation");

    private String title;

    private AliyunSmsTemplateEnum(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
