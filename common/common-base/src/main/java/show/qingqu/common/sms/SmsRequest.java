package show.qingqu.common.sms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author kam
 * @Desc
 */
@Data
@ToString
public class SmsRequest implements Serializable {

    private static final long serialVersionUID = -7103651892462843752L;

    @Pattern(regexp = "^1[345789]\\d{9}$", message = "手机号码格式不正确")
    @ApiModelProperty("手机号码")
    private String mobile;

    /**
     * 验证码
     */
    private String code;

    /**
     * 短信类型
     */
    private AliyunSmsTemplateEnum smsType;

    /**
     * 原因
     */
    private String reason;

    /**
     * 认证类型
     */
    private String auditType;

    /**
     * 状态
     */
    private String status;

    /**
     * 用户提现提醒参数传递
     */
    private DrawRecord drawRecord;

    /**
     * 支付成功提醒卖家参数传递
     */
    private PaySuccessSeller paySuccessSeller;

    /**
     * 支付成功提醒卖家 参数类
     */
    @Data
    public static class PaySuccessSeller implements Serializable {
        private static final long serialVersionUID = 8507737655507227124L;
        /**
         * 卖家
         */
        private String seller;
        /**
         * 买家
         */
        private String buyer;
        /**
         * 付款金额
         */
        private String money;

        public PaySuccessSeller() {

        }
    }

    @Data
    public static class DrawRecord implements Serializable {

        private static final long serialVersionUID = 3033085093043692169L;
        /**
         * 用户名称
         */
        private String userName;

        /**
         * 用户手机号
         */
        private String phone;

        /**
         * 提现金额
         */
        private String money;

        public DrawRecord() {

        }
    }
}
