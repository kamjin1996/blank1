package show.qingqu.common.utils;

import org.apache.commons.lang3.StringUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Base64工具类
 *
 * @author kam
 *
 */
public class Base64Util {

    public static String encode(String params) {
        if (StringUtils.isBlank(params)) {
            return "";
        }
        String paper = null;
        BASE64Encoder encoder = new BASE64Encoder();
        try {
            paper = encoder.encode(params.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return paper;
    }

    public static String decode(String params) {
        if (StringUtils.isBlank(params)) {
            return "";
        }
        String paper = null;
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            paper = new String(decoder.decodeBuffer(params), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return paper;
    }

}
