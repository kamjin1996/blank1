package show.qingqu.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * 坐标工具类
 */
public final class CoordinateUtil {

    /**
     * 经纬度校验
     *
     * @param longitude string
     * @param latitude  string
     * @return
     */
    public static boolean checkCoordinate(String longitude, String latitude) {
        if (StringUtils.isBlank(longitude) || StringUtils.isBlank(latitude)) {
            return false;
        }
        return longitude.matches(CoordinateRule.REG_LONGITUDE.title) == true ?
                latitude.matches(CoordinateRule.REG_LATITUDE.title) : false;
    }


    /**
     * 经纬度校验
     *
     * @param longitude double
     * @param latitude  double
     * @return
     */
    public static boolean checkCoordinate(Double longitude, Double latitude) {
        if (Objects.isNull(longitude) || Objects.isNull(latitude)) {
            return false;
        }
        String lon = longitude.toString();
        String lat = latitude.toString();
        return checkCoordinate(lon, lat);
    }


    public enum CoordinateRule {
        REG_LONGITUDE("((?:[0-9]|[1-9][0-9]|1[0-7][0-9])\\.([0-9]{0,6}))|((?:180)\\.([0]{0,6}))"),
        REG_LATITUDE("((?:[0-9]|[1-8][0-9])\\.([0-9]{0,6}))|((?:90)\\.([0]{0,6}))");

        public String title;

        CoordinateRule(String title) {
            this.title = title;
        }
    }
}
