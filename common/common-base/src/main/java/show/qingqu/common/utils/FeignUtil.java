package show.qingqu.common.utils;

import show.qingqu.common.exception.FeignServiceException;
import show.qingqu.common.result.Result;

/**
 * 服务调用辅助类
 */
public final class FeignUtil {

    /**
     * 处理服务调用结果
     *
     * @param result
     * @param <T>
     * @return
     */
    public static <T> T handle(Result<T> result) {
        if (result.getCode() != 200) {
            throw new FeignServiceException(result.getCode(), result.getMsg());
        }
        return result.getData();
    }
}
