package show.qingqu.common.utils;

/**
 * @auther: kam
 *
 * @description: 认证信息处理工具
 */
public class AuthUtils {

    /**
     * 实地认证枚举
     */
    public enum FieldType {

        /**
         * 已实地认证
         */
        FIELD("已实地认证"),

        /**
         * 与身份父名称对应的 "已实地认证"类型
         */
        REPAIR("实地认证修理厂"),
        SELLER("实地认证经销商");

        public String title;

        FieldType(String title) {
            this.title = title;
        }
    }

}
