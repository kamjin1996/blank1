package qrcode;

import lombok.Data;
import qrcode.enums.QRCommand;

/**
 * @author kam
 **/
@Data
public class QRcode {

    /**
     * 二维码指令
     */
    private QRCommand command;

    /**
     * 参数
     */
    private Object param;

    public QRcode(QRCommand command, Object param) {
        this.command = command;
        this.param = param;
    }
}
