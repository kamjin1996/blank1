package qrcode;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import qrcode.enums.BaseConst;
import qrcode.enums.QRCommand;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author kam
 **/
@Slf4j
public class QrCodeFactory {

    private QrCodeFactory() {
    }

    /**
     * 创建二维码信息
     *
     * @param command 指令
     * @param param   参数
     * @return 二维码信息
     */
    public static String build(QRCommand command, Object param) {
        QRcode qRcode = new QRcode(command, param);
        String res = "";
        try {
            res = URLEncoder.encode(JSON.toJSONString(qRcode), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("二维码url生成失败，原始信息：" + JSON.toJSONString(qRcode));
        }
        return BaseConst.BASE_URL + res;
    }

}
