package qrcode.enums;

/**
 * 询价推送记录用户类型 【商家、普通用户、客服】
 *
 * @author kam
 * @Desc
 */
public class EnquiryLogUserType {

    /**
     * 商家
     */
    public static final String SELLER_USER = "SELLER_USER";

    /**
     * 普通用户
     */
    public static final String MERCHANT_USER = "MERCHANT_USER";


    /**
     * 客服
     */
    public static final String SUPPORT_USER = "SUPPORT_USER";

}
