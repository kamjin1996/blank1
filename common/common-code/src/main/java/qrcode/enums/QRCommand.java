package qrcode.enums;

/**
 * @author kam
 **/
public enum QRCommand {

    /**
     * 登录
     */
    LOGIN("登录"),

    /**
     * 店铺
     */
    STORE("店铺信息"),

    /**
     * 推广码
     */
    SPREAD("推广码"),

    /**
     * 个人中心
     */
    PERSONAL("个人中心");

    private String command;

    QRCommand(String command) {
        this.command = command;
    }
}
