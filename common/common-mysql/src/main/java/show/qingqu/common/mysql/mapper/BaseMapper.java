package show.qingqu.common.mysql.mapper;


import show.qingqu.common.mysql.model.BaseModel;

public interface BaseMapper<T extends BaseModel> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {

}