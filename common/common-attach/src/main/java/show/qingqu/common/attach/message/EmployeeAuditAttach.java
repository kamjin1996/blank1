package show.qingqu.common.attach.message;

import show.qingqu.common.attach.AttachMsgClassify;

import java.util.HashMap;
import java.util.Map;

/**
 * 汽修厂审核消息通知
 */
public class EmployeeAuditAttach extends Attach {

    /**
     * 当前用户ID
     */
    public Long userId;

    @Override
    public Map getAttach() {
        Map<String, Object> attachMap = new HashMap<>();
        attachMap.put("classify", this.classify);
        attachMap.put("userId", this.userId);
        attachMap.put("title", this.title);
        attachMap.put("content", this.content);
        return attachMap;
    }

    /**
     * 创建消息
     *
     * @param userId
     * @param auditResult
     */
    public EmployeeAuditAttach(Long userId, boolean auditResult) {
        super();
        super.toUserIds.add(userId);
        this.userId = userId;
        if (auditResult) {
            super.classify = AttachMsgClassify.REPAIR_AUTH_SUCCESS;
            this.title = "汽修厂实地认证审核成功通知";
            this.content = "尊敬的用户：您的“汽修厂实地认证”审核成功，点击查看详情>>";
        } else {
            super.classify = AttachMsgClassify.REPAIR_AUTH_FAILURE;
            this.title = "汽修厂实地认证审核失败通知";
            this.content = "尊敬的用户：您的“汽修厂实地认证”审核失败，点击查看详情>>";
        }
    }

    public EmployeeAuditAttach() {
    }
}