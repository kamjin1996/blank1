package show.qingqu.common.attach.message;


import show.qingqu.common.attach.AttachMsgClassify;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统新闻通知消息
 */
public class NewsAttach extends Attach {

    /**
     * 新闻ID
     */
    public Long newsId;

    /**
     * banner
     */
    public String banner;

    @Override
    public Map getAttach() {
        Map<String, Object> attachMap = new HashMap<>();
        attachMap.put("classify", this.classify);
        attachMap.put("banner", this.banner);
        attachMap.put("newsId", this.newsId);
        attachMap.put("title", this.title);
        attachMap.put("content", this.content);
        return attachMap;
    }

    /**
     * 创建消息
     *
     * @param userIds
     * @param newsId
     * @param banner
     * @param title
     * @param content
     */
    public NewsAttach(List<Long> userIds, Long newsId, String banner, String title, String content) {
        super();
        super.classify = AttachMsgClassify.SYS_NEWS;
        super.toUserIds = userIds;
        this.newsId = newsId;
        this.banner = banner;
        this.title = title;
        this.content = content;
    }

    public NewsAttach() {
    }
}
