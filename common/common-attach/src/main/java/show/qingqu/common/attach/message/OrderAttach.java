package show.qingqu.common.attach.message;


import show.qingqu.common.attach.AttachMsgClassify;

import java.util.HashMap;
import java.util.Map;

/**
 * 报价发布系统消息
 */
public class OrderAttach extends Attach {

    private static final long serialVersionUID = 139671241769284523L;
    /**
     * 订单编号
     */
    public String tradeNo;

    /**
     * 退款单ID
     */
    public Long refundId;

    /**
     * logo
     */
    public String logo;

    /**
     * 地址
     */
    public String address;

    /**
     * 名称
     */
    public String name;

    /**
     * 总价
     */
    public Long amount;
    /**
     * 商品信息
     */
    public String goodsInfo;
    /**
     * 品牌
     */
    public String brand;

    /**
     * 消息类型
     */
    public MsgType msgType;

    public enum MsgType {

        /************************************** 买家消息 *******************************/
        /**
         * 订单已发货去收货
         */
        BUYER_SIPPING("已发货（等待收货）"),

        /**
         * 交易成功
         */
        BUYER_TAKE("已收货（交易成功）"),

        /**
         * 售后已申请等待卖家处理
         */
        BUYER_REFUND("售后已申请等待卖家处理"),


        /**
         * 售后已成功
         */
        BUYER_REFUND_OK("售后已成功"),


        /**
         * 卖家同意退货请发货
         */
        BUYER_REFUND_CONSENT("卖家同意退货请发货"),

        /**
         * 售后失败(卖家拒绝退款)
         */
        BUYER_REFUND_REJECT("售后失败(卖家拒绝退款)"),

        /**
         * 售后失败(卖家拒绝退货)
         */
        BUYER_RETURN_REJECT("售后失败(卖家拒绝退货)"),

        /**
         * 客服已介入
         */
        BUYER_SERVICE_INVO("客服已介入"),

        /**
         * 订单调节成功
         */
        BUYER_SERVICE_OK("订单客服调节成功"),

        /************************************** 卖家消息 *******************************/
        /**
         * 订单已付款去发货
         */
        SELLER_PAY("去发货（买家已付款）"),

        /**
         * 交易成功买家已收货
         */
        SELLER_TAKE("买家已收货（交易成功）"),

        /**
         * 卖家订单结算
         */
        SETTLE("货款已到账（去提现）"),

        /**
         * 买家申请售后请查看
         */
        SELLER_REFUND("买家申请售后请查看"),

        /**
         * 买家已退货去查收
         */
        SELLER_RETURN("买家已退货去查收"),

        /**
         * 退款成功
         */
        SELLER_REFUND_OK("退款成功"),

        /**
         * 订单未发货已退款
         */
        SELLER_NOT_SIPP_RETURN("已退款（订单未发货）"),

        /**
         * 客服已介入
         */
        SELLER_SERVICE_INVO("客服已介入"),

        /**
         * 订单调节成功
         */
        SELLER_SERVICE_OK("订单调节成功");

        private String title;

        MsgType(String title) {
            this.title = title;
        }
    }

    @Override
    public Map getAttach() {
        Map<String, Object> attachMap = new HashMap<>();
        attachMap.put("classify", this.classify);
        attachMap.put("tradeNo", this.tradeNo);
        attachMap.put("refundId", this.refundId);
        attachMap.put("logo", this.logo);
        attachMap.put("msgType", msgType);
        attachMap.put("title", this.title);
        attachMap.put("content", this.content);
        attachMap.put("amount", this.amount);
        attachMap.put("address", this.address);
        attachMap.put("name", this.name);
        attachMap.put("brand", this.brand);
        attachMap.put("goodsInfo", this.goodsInfo);
        return attachMap;
    }

    /**
     * 订单消息
     *
     * @param userId
     * @param tradeNo
     * @param logo
     */
    public OrderAttach(Long userId, String tradeNo,
                       String logo, Long amount, String brand,
                       String goodsInfo, String name,
                       String address, MsgType msgType) {
        super();
        super.classify = AttachMsgClassify.ORDER;
        super.toUserIds.add(userId);
        this.tradeNo = tradeNo;
        this.amount = amount;
        this.logo = logo;
        this.msgType = msgType;
        this.address = address;
        this.name = name;
        this.brand = brand;
        this.goodsInfo = goodsInfo;
        this.title = msgType.title;
        this.content = msgType.title;
    }

    /**
     * 退款消息
     *
     * @param userId
     * @param refundId
     * @param logo
     */
    public OrderAttach(Long userId, Long refundId,
                       String logo, Long amount, String brand,
                       String goodsInfo, String name,
                       String address, MsgType msgType) {
        super();
        super.classify = AttachMsgClassify.ORDER;
        super.toUserIds.add(userId);
        this.refundId = refundId;
        this.amount = amount;
        this.logo = logo;
        this.msgType = msgType;
        this.title = msgType.title;
        this.address = address;
        this.name = name;
        this.brand = brand;
        this.goodsInfo = goodsInfo;
        this.title = msgType.title;
        this.content = msgType.title;
    }

    public OrderAttach() {
    }
}
