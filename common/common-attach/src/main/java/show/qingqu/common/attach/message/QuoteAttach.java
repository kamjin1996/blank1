package show.qingqu.common.attach.message;


import show.qingqu.common.attach.AttachMsgClassify;

import java.util.HashMap;
import java.util.Map;

/**
 * 报价发布系统消息
 */
public class QuoteAttach extends Attach {

    /**
     * 新闻ID
     */
    public Long enquiryId;

    /**
     * logo
     */
    public String logo;

    /**
     * 新增或者修改
     */
    public boolean addOrUpdate;

    /**
     * 报价单类型
     */
    public String type;

    @Override
    public Map getAttach() {
        Map<String, Object> attachMap = new HashMap<>();
        attachMap.put("classify", this.classify);
        attachMap.put("enquiryId", this.enquiryId);
        attachMap.put("logo", this.logo);
        attachMap.put("addOrUpdate", this.addOrUpdate);
        attachMap.put("type", type);
        attachMap.put("title", this.title);
        attachMap.put("content", this.content);
        return attachMap;
    }

    /**
     * 消息创建
     *
     * @param userId
     * @param enquiryId
     * @param logo
     * @param brand
     * @param addOrUpdate
     */
    public QuoteAttach(Long userId, Long enquiryId, String logo, String brand, boolean addOrUpdate, String type) {
        super();
        super.classify = AttachMsgClassify.QUOTE;
        super.toUserIds.add(userId);
        this.enquiryId = enquiryId;
        this.logo = logo;
        this.type = type;
        this.addOrUpdate = addOrUpdate;
        if (addOrUpdate) {
            this.content = "您发布询价“" + brand + "”品牌车型相关配件，已有经销商为您报价，点击查看详情>>";
            this.title = "汽配经销商给您报价";
        } else {
            this.content = "您发布询价“" + brand + "”品牌车型相关配件，经销商已更新了报价，点击查看详情>>";
            this.title = "汽配经销商修改了报价";
        }
    }

    public QuoteAttach() {
    }
}
