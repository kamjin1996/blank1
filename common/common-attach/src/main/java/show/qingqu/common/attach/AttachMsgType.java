package show.qingqu.common.attach;

/**
 * 系统通知类型
 *
 * @author kam
 *
 */
public enum AttachMsgType {


    /**
     * 个人
     */
    PERSONAL(0),

    /**
     * 群
     */
    GROUP(1),

    /**
     * 批量发送
     */
    BATCH(3);

    public int type;

    AttachMsgType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
