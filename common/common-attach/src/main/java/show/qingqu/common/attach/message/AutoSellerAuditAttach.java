package show.qingqu.common.attach.message;

import show.qingqu.common.attach.AttachMsgClassify;

import java.util.HashMap;
import java.util.Map;

/**
 * 汽配经销商审核通知消息
 */
public class AutoSellerAuditAttach extends Attach {

    /**
     * 汽配经销商ID
     */
    public Long autoSellerId;

    @Override
    public Map getAttach() {
        HashMap<String, Object> attachMap = new HashMap<>();
        attachMap.put("classify", this.classify);
        attachMap.put("autoSellerId", this.autoSellerId);
        attachMap.put("title", this.title);
        attachMap.put("content", this.content);
        return attachMap;
    }

    /**
     * 创建消息
     *
     * @param autoSellerId 汽配经销商ID
     * @param auditResult  审核结果
     */
    public AutoSellerAuditAttach(Long userId, Long autoSellerId, boolean auditResult) {
        super();
        super.toUserIds.add(userId);
        this.autoSellerId = autoSellerId;
        if (auditResult) {
            super.classify = AttachMsgClassify.SELLER_AUTH_SUCCESS;
            this.title = "经销商入驻审核通过通知";
            this.content = "尊敬的用户:您的“商家入驻申请”审核已通过，点击查看详情>>";
        } else {
            super.classify = AttachMsgClassify.SELLER_AUTH_FAILURE;
            this.title = "经销商入驻审核未通过通知";
            this.content = "尊敬的用户:您的“商家入驻申请”审核未通过，点击查看详情>>";
        }
    }

    public AutoSellerAuditAttach() {
    }
}