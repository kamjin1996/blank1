package show.qingqu.common.attach.message;


import show.qingqu.common.attach.AttachMsgClassify;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 询价发布系统消息
 */
public class EnquiryAttach extends Attach {

    /**
     * 新闻ID
     */
    public Long enquiryId;

    /**
     * logo
     */
    public String logo;

    /**
     * 询价单类型
     */
    public String type;

    @Override
    public Map getAttach() {
        Map<String, Object> attachMap = new HashMap<>();
        attachMap.put("classify", this.classify);
        attachMap.put("enquiryId", this.enquiryId);
        attachMap.put("logo", this.logo);
        attachMap.put("type", this.type);
        attachMap.put("title", this.title);
        attachMap.put("content", this.content);
        return attachMap;
    }

    /**
     * 消息创建
     *
     * @param userIds
     * @param enquiryId
     * @param logo
     * @param brand
     */
    public EnquiryAttach(List<Long> userIds, Long enquiryId, String logo, String brand, String address, String part, String type) {
        super();
        super.classify = AttachMsgClassify.ENQUIRY;
        super.toUserIds = userIds;
        this.enquiryId = enquiryId;
        this.logo = logo;
        this.type = type;
        // 示例
        String content = "杭州市 江干区，正在采购“马自达阿特兹”前嘴（前脸）相关配件，与您的经营范围相匹配，点击快去报价>>";
        if (StringUtils.isBlank(part)) {
            content = address + "，正在采购“" + brand + "”相关配件，与您的经营范围相匹配，点击快去报价>>";
        } else {
            content = address + "，正在采购“" + brand + "”" + part + "相关配件，与您的经营范围相匹配，点击快去报价>>";
        }
        this.content = content;
        this.title = "有买家发布了新的询价需求";
    }

    public EnquiryAttach() {
    }
}
