package show.qingqu.common.attach;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统通知接收参数类
 *
 * @author kam
 *
 * @Desc
 */
public class AttachMsgLogRequest implements Serializable {

    private static final long serialVersionUID = -3320454776544602439L;
    /**
     * 发送者userId
     */
    private Long fromUserId;

    /**
     * 接收者userId
     */
    private List<Long> toUserIds;

    /**
     * 通知分类[系统通知、报价通知、审核通知等]
     */
    private AttachMsgClassify classify;

    /**
     * 系统通知类型
     */
    private AttachMsgType msgType;

    /**
     * 系统通知内容
     */
    private String msgBody;

    /**
     * 创建时间
     */
    private Date createTime;


    /**
     * 创建时间
     */
    private Object pushcontent;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public List<Long> getToUserIds() {
        return toUserIds;
    }

    public void setToUserIds(List<Long> toUserIds) {
        this.toUserIds = toUserIds;
    }

    public AttachMsgClassify getClassify() {
        return classify;
    }

    public void setClassify(AttachMsgClassify classify) {
        this.classify = classify;
    }

    public AttachMsgType getMsgType() {
        return msgType;
    }

    public void setMsgType(AttachMsgType msgType) {
        this.msgType = msgType;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Object getPushcontent() {
        return pushcontent;
    }

    public void setPushcontent(Object pushcontent) {
        this.pushcontent = pushcontent;
    }
}
