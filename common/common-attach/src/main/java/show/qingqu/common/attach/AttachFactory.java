package show.qingqu.common.attach;

import show.qingqu.common.attach.message.*;

import java.util.List;

/**
 * 系统通知发送工厂
 */
public final class AttachFactory {

    /**
     * 新闻消息
     *
     * @param userIds
     * @param newsId
     * @param banner
     * @param title
     * @param content
     * @return
     */
    public static Attach newsAttach(List<Long> userIds, Long newsId, String banner, String title, String content) {
        return new NewsAttach(userIds, newsId, banner, title, content);
    }

    /**
     * 询价消息
     *
     * @param userIds
     * @param enquiryId
     * @param logo
     * @param brand
     * @param type
     * @return
     */
    public static Attach enquiryAttach(List<Long> userIds, Long enquiryId, String logo, String brand, String address, String part, String type) {
        return new EnquiryAttach(userIds, enquiryId, logo, brand, address, part, type);
    }

    /**
     * 报价消息
     *
     * @param userId
     * @param enquiryId
     * @param logo
     * @param brand
     * @return
     */
    public static Attach quoteAttach(Long userId, Long enquiryId, String logo, String brand, boolean addOrUpdate, String type) {
        return new QuoteAttach(userId, enquiryId, logo, brand, addOrUpdate, type);
    }

    /**
     * 汽配经销商审核消息
     *
     * @param userId
     * @param autoSellerId
     * @param auditResult
     * @return
     */
    public static Attach autoSellerAuditAttach(Long userId, Long autoSellerId, boolean auditResult) {
        return new AutoSellerAuditAttach(userId, autoSellerId, auditResult);
    }


    /**
     * 修理厂审核消息
     *
     * @param userId
     * @param auditResult
     * @return
     */
    public static Attach employeeAuditAttach(Long userId, boolean auditResult) {
        return new EmployeeAuditAttach(userId, auditResult);
    }

}
