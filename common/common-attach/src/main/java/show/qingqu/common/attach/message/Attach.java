package show.qingqu.common.attach.message;


import show.qingqu.common.attach.AttachMsgClassify;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 系统消息通知基础类
 *
 * @author kam
 *
 * @Desc
 */
public abstract class Attach implements Serializable {

    public static final long serialVersionUID = -3320454776544602439L;

    /**
     * 接收者userId
     */
    public List<Long> toUserIds = new ArrayList<>();

    /**
     * 通知分类
     */
    public AttachMsgClassify classify;

    /**
     * 标题
     */
    public String title;

    /**
     * 内容
     */
    public String content;

    /**
     * 获取自定义消息内容
     *
     * @return
     */
    public abstract Map getAttach();

}
