package show.qingqu.common.attach;

/**
 * 系统通知分类
 *
 * @author kam
 *
 */
public enum AttachMsgClassify {

    /**
     * 系统通知
     */
    SYS_NEWS("系统新闻"),
    /**
     * 报价
     */
    QUOTE("报价"),

    /**
     * 询价
     */
    ENQUIRY("询价"),

    /**
     * 汽配经销商审核失败
     */
    SELLER_AUTH_FAILURE("汽配经销商审核失败"),

    /**
     * 汽配经销商审核成功
     */
    SELLER_AUTH_SUCCESS("汽配经销商审核成功"),

    /**
     * 汽修厂审核结果通知
     */
    REPAIR_AUTH_FAILURE("汽修厂审核失败"),

    /**
     * 汽修厂审核结果通知
     */
    REPAIR_AUTH_SUCCESS("汽修厂审核成功"),

    /**
     * 订单
     */
    ORDER("订单");


    public String title;

    AttachMsgClassify(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
