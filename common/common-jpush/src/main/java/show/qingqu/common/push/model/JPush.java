package show.qingqu.common.push.model;

import java.io.Serializable;
import java.util.List;

/**
 * 极光推送参数接收类
 *
 * @author kam
 * @Desc
 */
public class JPush implements Serializable {

    private static final long serialVersionUID = 2808155383219149395L;

    /**
     * 操作者ID集合
     */
    private List<Long> userIds;

    /**
     * 指令
     */
    private Command command;

    /**
     * 参数
     */
    private Object params;

    /**
     * 标题
     */
    private String title;

    /**
     * 提示内容
     */
    private String alert;

    public JPush() {
    }

    public JPush(List<Long> userIds, Command command, String title, String alert) {
        this.userIds = userIds;
        this.command = command;
        this.title = title;
        this.alert = alert;
    }

    public JPush(List<Long> userIds, Command command, String alert) {
        this.userIds = userIds;
        this.command = command;
        this.alert = alert;
    }

    public JPush(List<Long> userIds, Command command, Object params, String title, String alert) {
        this.userIds = userIds;
        this.command = command;
        this.params = params;
        this.title = title;
        this.alert = alert;
    }

    public JPush(List<Long> userIds, Command command, Object params, String alert) {
        this.userIds = userIds;
        this.command = command;
        this.params = params;
        this.alert = alert;
    }


    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
