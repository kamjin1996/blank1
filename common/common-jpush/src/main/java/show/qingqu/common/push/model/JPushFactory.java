package show.qingqu.common.push.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 静态工厂方法
 *
 * @author kam
 */
public final class JPushFactory {

    private JPushFactory() {
    }

    /**
     * 创建Jpush对象
     *
     * @param userIds 推送用户ID
     * @param command 指令
     * @param title   推送标题
     * @param alert   提示内容
     * @param params  附加参数
     * @return
     */
    public static JPush build(List<Long> userIds, Command command, String title, String alert, Object params) {
        return new JPush(userIds, command, params, title, alert);
    }

    /**
     * 创建Jpush对象
     *
     * @param userIds 推送用户ID
     * @param command 指令
     * @param title   推送标题
     * @param alert   提示内容
     * @return
     */
    public static JPush build(List<Long> userIds, Command command, String title, String alert) {
        return new JPush(userIds, command, title, alert);
    }


    /**
     * 创建Jpush对象
     *
     * @param userIds 推送用户ID
     * @param command 指令
     * @param alert   提示内容
     * @return
     */
    public static JPush build(List<Long> userIds, Command command, String alert) {
        return new JPush(userIds, command, alert);
    }


    /**
     * 创建Jpush对象
     *
     * @param userId  推送用户ID
     * @param command 指令
     * @param title   推送标题
     * @param alert   提示内容
     * @param params  附加参数
     * @return
     */
    public static JPush build(Long userId, Command command, String title, String alert, Object params) {
        List<Long> userIds = new ArrayList();
        userIds.add(userId);
        return new JPush(userIds, command, params, title, alert);
    }


    /**
     * 创建Jpush对象
     *
     * @param userId  推送用户ID
     * @param command 指令
     * @param title   推送标题
     * @param alert   提示内容
     * @return
     */
    public static JPush build(Long userId, Command command, String title, String alert) {
        List<Long> userIds = new ArrayList();
        userIds.add(userId);
        return new JPush(userIds, command, title, alert);
    }


    /**
     * 创建Jpush对象
     *
     * @param userId  推送用户ID
     * @param command 指令
     * @param alert   提示内容
     * @return
     */
    public static JPush build(Long userId, Command command, String alert) {
        List<Long> userIds = new ArrayList();
        userIds.add(userId);
        return new JPush(userIds, command, alert);
    }


}
