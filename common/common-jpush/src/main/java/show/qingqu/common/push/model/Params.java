package show.qingqu.common.push.model;

/**
 * @author kam
 * @Desc
 */
public class Params {

    private Command command;

    private Object params;

    public Params(Command command, Object params) {
        this.command = command;
        this.params = params;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }
}
