package show.qingqu.common.push.model;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * 极光推送自定义消息
 *
 * @author kam
 */
public class JPushObject {

    /*
     * 消息别名
     */
    private String[] alias;

    /**
     * 标题
     */
    private String title;

    /**
     * 提示内容
     */
    private String alert;

    /**
     * 参数
     */
    private Object params;


    public JPushObject() {
    }

    ;

    public JPushObject(JPush jPush) {
        // TODO 处理  userId 转换为 alias
        List<Long> userIds = jPush.getUserIds();
        String[] alias = new String[userIds.size()];
        for (int i = 0; i < userIds.size(); i++) {
            alias[i] = userIds.get(i).toString();
        }
        this.alias = alias;
        this.alert = jPush.getAlert();
        this.title = jPush.getTitle();
        this.params = new Params(jPush.getCommand(), jPush.getParams());
    }

    public String[] getAlias() {
        return alias;
    }

    public void setAlias(String[] alias) {
        this.alias = alias;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

}
