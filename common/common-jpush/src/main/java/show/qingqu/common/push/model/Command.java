package show.qingqu.common.push.model;

/**
 * 指令类型
 *
 * @author kam
 */
public enum Command {

    PERSONAL("个人信息"), PROMOTE("推广"), SELLER_AUDIT("商家审核"), LOGIN("登录"), ENQUIRY("我的询价"), QUOTE("我的报价");

    private String title;

    Command(String title) {
        this.title = title;
    }
}
