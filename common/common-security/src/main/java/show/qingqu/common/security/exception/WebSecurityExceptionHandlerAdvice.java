package show.qingqu.common.security.exception;

import show.qingqu.common.exception.handler.ExceptionHandlerAdvice;
import show.qingqu.common.result.BaseResponse;
import show.qingqu.common.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * @author kam
 *
 * @Description: 异常处理
 */
@Slf4j
@RestControllerAdvice
public class WebSecurityExceptionHandlerAdvice extends ExceptionHandlerAdvice {
    /**
     * 用户未认证登录 401
     *
     * @param e
     * @return
     */
    @ExceptionHandler(AccessDeniedException.class)
    public Result AccessDeniedException(AccessDeniedException e) {
        log.debug("\n【异常类别】用户未认证登录", e);
        return BaseResponse.getUnauthorizedResult();
    }
}
