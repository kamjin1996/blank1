package com.xxl.job.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by ccb on 2017/1/20.
 */
@EnableDiscoveryClient
@SpringBootApplication
@PropertySource(value = "classpath:xxl-job-admin.properties")
public class XllJobAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(XllJobAdminApplication.class, args);
    }
}
